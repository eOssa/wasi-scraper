#!/bin/bash

if ! command -v php &> /dev/null; then
    echo "PHP 7.3 not installed. Installing..."
    sudo yum install -y epel-release --quiet
    sudo yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm --quiet
    sudo yum install -y yum-utils --quiet
    sudo yum-config-manager --enable remi-php73 --quiet
    sudo yum update --quiet
    sudo yum install -y php73 php73-php-bcmath php73-php-mbstring php73-php-pdo php73-php-xml php73-php-pecl-mongodb php73-php-pecl-zip --quiet
    sudo ln /usr/bin/php73 /usr/bin/php
fi

if ! command -v mongo &> /dev/null; then
    echo "MongoDB not installed. Installing..."
    sudo cp mongodb-org-4.4.repo /etc/yum.repos.d/mongodb-org-4.4.repo
    sudo yum install -y mongodb-org --quiet
    sudo systemctl start mongod
    sudo systemctl enable mongod
fi

if ! command -v composer &> /dev/null; then
    echo "Composer not installed. Installing..."
    EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

    if [ "$EXPECTED_CHECKSUM" == "$ACTUAL_CHECKSUM" ]; then
        mkdir /home/eossa93/.local
        mkdir /home/eossa93/.local/bin
        php composer-setup.php --install-dir=/home/eossa93/.local/bin --filename=composer --quiet
        rm composer-setup.php
    fi
fi

if ! command -v wget &> /dev/null; then
    echo "wget not installed. Installing..."
    sudo yum install -y wget --quiet
fi

if ! command -v google-chrome &> /dev/null; then
    echo "Google Chrome not installed. Installing..."
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm --quiet
    sudo yum install -y ./google-chrome-stable_current_*.rpm --quiet
fi

if command -v unzip &> /dev/null; then
    echo "unzip not installed. Installing..."
    sudo yum install -y unzip --quiet
fi

if command -v composer &> /dev/null; then
    composer run post-root-package-install
    composer install
    composer run post-create-project-cmd
    php /home/eossa93/wasi-scraper/artisan migrate
    php /home/eossa93/wasi-scraper/artisan dusk:chrome-driver
fi

if ! command sudo crontab -u eossa93 -l &> /dev/null; then
    echo "Estblishing crontab job..."
    (crontab -l 2>/dev/null; echo "* * * * * php /home/eossa93/wasi-scraper/artisan schedule:run") | crontab -
fi

echo "Instalation finished."
