<?php

namespace Tests\Browser;

use Illuminate\Support\Facades\Cache;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Fincaraiz\ListPage;
use Tests\DuskTestCase;

class FincaraizTest extends DuskTestCase
{
    public function testList()
    {
        $this->browse(function (Browser $browser) {
            $currentPage = Cache::get('fincaraiz.list.page', 1);
            $total = 2251;
            foreach (range($currentPage, $total) as $page) {
                Cache::forever('fincaraiz.list.page', $page);
                $percent = round($page / $total * 100, 2);
                dump("$page / $total ($percent%)");
                $browser->visit(new ListPage($page))->goToDetails();
            }
        });
    }
}
