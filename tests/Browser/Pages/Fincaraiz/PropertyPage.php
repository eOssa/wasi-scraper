<?php

namespace Tests\Browser\Pages\Fincaraiz;

use App\Helpers;
use App\Models\{FincaraizProperty, Property};
use Illuminate\Support\Str;
use Laravel\Dusk\Browser;

class PropertyPage extends Base
{
    /**
     * The property repository.
     *
     * @var \App\Contracts\Repository\PropertyRepositoryInterface
     */
    private $propertyRepository;

    public $url;

    public function __construct($url)
    {
        $this->url = $url;
        $this->propertyRepository = app(\App\Contracts\Repository\PropertyRepositoryInterface::class);
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->url;
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        // $browser->assertUrlIs($this->url());
    }

    public function extractData(Browser $browser)
    {
        // Exclude projects
        if ($this->getCondition($browser) === 'Proyecto Nuevo') {
            return false;
        }
        if ($this->propertyRepository->existsByReference($this->source . ':' . $this->getReference($browser))) {
            return false;
        }
        $fincaraizProperty = new FincaraizProperty();
        $fincaraizProperty->setAddress($this->getAddress($browser))
            ->setAdministrationFee($this->getAdministrationFee($browser))
            ->setArea($this->getArea($browser))
            ->setBathrooms($this->getBathrooms($browser))
            ->setBedrooms($this->getBedrooms($browser))
            ->setBuiltArea($this->getBuiltArea($browser))
            ->setCity($this->getCity($browser))
            ->setCondition($this->getCondition($browser))
            ->setCountry('Colombia')
            ->setFloor($this->getFloor($browser))
            ->setGarages($this->getGarages($browser))
            ->setLatitude($this->getLatitude($browser))
            ->setLongitude($this->getLongitude($browser))
            ->setNeighborhood($this->getNeighborhood($browser))
            ->setRating($this->getRating($browser))
            ->setReference($this->source . ':' . $this->getReference($browser))
            ->setRegion($this->getRegion($browser))
            ->setPrice($this->getPrice($browser))
            ->setPrivateArea($this->getPrivateArea($browser))
            ->setTitle($this->getTitle($browser))
            ->setTradeType($this->getTradeType($browser))
            ->setType($this->getType($browser))
            ->setVideo($this->getVideo($browser))
            ->setVisits($this->getVisits($browser))
            ->setZone($this->getZone($browser));
        $property = new Property();
        $property->setAddress($fincaraizProperty->getAddress())
            ->setAdministrationFee($fincaraizProperty->getAdministrationFee())
            ->setArea($fincaraizProperty->getAreaValue())
            ->setAreaUnit($fincaraizProperty->getAreaUnit())
            ->setAvailability($fincaraizProperty->getAvailability())
            ->setBathrooms($fincaraizProperty->getBathrooms())
            ->setBedrooms($fincaraizProperty->getBedrooms())
            ->setBuiltArea($fincaraizProperty->getBuiltAreaValue())
            ->setBuiltAreaUnit($fincaraizProperty->getBuiltAreaUnit())
            ->setBuiltDate($fincaraizProperty->getBuiltDate())
            ->setCityId($fincaraizProperty->getCityId())
            ->setCommissionType($fincaraizProperty->getCommissionType())
            ->setCommissionValue($fincaraizProperty->getCommissionValue())
            ->setCondition($fincaraizProperty->getCondition())
            ->setCountryId($fincaraizProperty->getCountryId())
            ->setCurrencyId($fincaraizProperty->getCurrencyId())
            ->setDate($fincaraizProperty->getDate())
            ->setFloor($fincaraizProperty->getFloor())
            ->setFurnished($fincaraizProperty->getFurnished())
            ->setLocationId($fincaraizProperty->getLocationId())
            ->setLogSalePrice($fincaraizProperty->getLogSalePrice())
            ->setGarages($fincaraizProperty->getGarages())
            ->setMap($fincaraizProperty->getMap())
            ->setMapPublishOption($fincaraizProperty->getMapPublishOption())
            ->setMaxDate($fincaraizProperty->getMaxDate())
            ->setPrivateArea($fincaraizProperty->getPrivateAreaValue())
            ->setPrivateAreaUnit($fincaraizProperty->getPrivateAreaUnit())
            ->setRating($fincaraizProperty->getRating())
            ->setReference($fincaraizProperty->getReference())
            ->setRegionId($fincaraizProperty->getRegionId())
            ->setRentPrice($fincaraizProperty->getRentPrice())
            ->setRentType($fincaraizProperty->getRentType())
            ->setSalePrice($fincaraizProperty->getSalePrice())
            ->setSharedInNetwork($fincaraizProperty->getSharedInNetwork())
            ->setSoldPrice($fincaraizProperty->getSoldPrice())
            ->setStatus($fincaraizProperty->getStatus())
            ->setTitle($fincaraizProperty->getTitle())
            ->setTradeTypeId($fincaraizProperty->getTradeTypeId())
            ->setType('')
            ->setTypeId($fincaraizProperty->getTypeId())
            ->setVideo($fincaraizProperty->getVideo())
            ->setVisits($fincaraizProperty->getVisits())
            ->setZoneId($fincaraizProperty->getZoneId());
        if ($property->getRegionId() === 0) {
            $property->setRegion($fincaraizProperty->getRegion());
        }
        if ($property->getCityId() === 0) {
            $property->setCity($fincaraizProperty->getCity());
        }
        $property->save();
    }

    public function getAddress(Browser $browser)
    {
        return $this->getFromAdvertScript($browser, 'Address');
    }

    public function getAdministrationFee(Browser $browser)
    {
        return Helpers::extractNumbers($this->getFeature2($browser, 'Admón'));
    }

    public function getArea(Browser $browser)
    {
        $value = $this->getFeature2($browser, 'Área:');
        if ($value) {
            list($label, $value) = explode("\n", $value);
        }
        return $value;
    }

    public function getBathrooms(Browser $browser)
    {
        return Helpers::extractNumbers($this->textOrNull($browser, '@bathrooms'));
    }

    public function getBedrooms(Browser $browser)
    {
        return Helpers::extractNumbers($this->textOrNull($browser, '@bedrooms'));
    }

    public function getBuiltArea(Browser $browser)
    {
        $value = $this->getFeature2($browser, 'Área Const.:');
        if ($value) {
            list($label, $value) = explode("\n", $value);
        }
        return $value;
    }

    public function getCity(Browser $browser)
    {
        list($region, $city) = $this->getLocation($browser);
        return $city;
    }

    public function getCondition(Browser $browser)
    {
        return trim($this->textOrNull($browser, '@status'));
    }

    public function getFeatures2(Browser $browser)
    {
        return $browser->elements('@features2');
    }

    public function getFeature2(Browser $browser, $name)
    {
        $elements = $this->getFeatures2($browser);
        foreach ($elements as $element) {
            if (Str::contains($element->getText(), $name)) {
                return $element->getText();
            }
        }
        return null;
    }

    public function getFloor(Browser $browser)
    {
        $value = $this->getFeature2($browser, 'Piso No:');
        if ($value) {
            list($label, $value) = explode("\n", $value);
            $value = trim($value, 'º');
        }
        return $value;
    }

    public function getFromAdvertScript(Browser $browser, $key)
    {
        return head($browser->script("return sfAdvert.$key.toString()"));
    }

    public function getGarages(Browser $browser)
    {
        return Helpers::extractNumbers($this->textOrNull($browser, '@garages'));
    }

    public function getLocation(Browser $browser)
    {
        $elements = $browser->elements('@location');
        $location = [];
        foreach ($elements as $element) {
            if ($element->getText() !== 'Inicio') {
                $location[] = $element->getText();
            }
        }
        return $location;
    }

    public function getLatitude(Browser $browser)
    {
        return $this->getFromAdvertScript($browser, 'Latitude');
    }

    public function getLongitude(Browser $browser)
    {
        return $this->getFromAdvertScript($browser, 'Longitude');
    }

    public function getNeighborhood(Browser $browser)
    {
        list($region, $city, $zone, $neighborhood) = $this->getLocation($browser);
        return $neighborhood;
    }

    public function getPrice(Browser $browser)
    {
        return Helpers::extractNumbers($this->textOrNull($browser, '@price'));
    }

    public function getPrivateArea(Browser $browser)
    {
        $value = $this->getFeature2($browser, 'Área privada:');
        if ($value) {
            list($label, $value) = explode("\n", $value);
        }
        return $value;
    }

    public function getRating(Browser $browser)
    {
        $value = $this->getFeature2($browser, 'Estrato');
        if ($value) {
            list($label, $value) = explode("\n", $value);
            $value = trim($value, 'º');
        }
        return $value;
    }

    public function getReference(Browser $browser)
    {
        return $this->textOrNull($browser, '@reference');
    }

    public function getRegion(Browser $browser)
    {
        list($region) = $this->getLocation($browser);
        return $region;
    }

    public function getTitle(Browser $browser)
    {
        list($title) = explode("\n", $this->textOrNull($browser, '@title'));
        return $title;
    }

    public function getTradeType(Browser $browser)
    {
        list($type, $tradeType) = explode(' en ', $this->getTitle($browser));
        return $tradeType;
    }

    public function getType(Browser $browser)
    {
        list($type) = explode(' en ', $this->getTitle($browser));
        return $type;
    }

    public function getVideo(Browser $browser)
    {
        $element = $browser->element('@video');
        if ($element) {
            $onClick = $element->getAttribute('onclick');
            $video = $onClick;
            $host = 'https://www.youtube.com/watch?v=';
            return $host . str_replace(['PopupVideo(\'', '\')'], [''], $video);
        }
        return '';
    }

    public function getVisits(Browser $browser)
    {
        return $this->textOrNull($browser, '@visits');
    }

    public function getZone(Browser $browser)
    {
        list($region, $city, $zone) = $this->getLocation($browser);
        return $zone;
    }

    public function textOrNull(Browser $browser, $selector)
    {
        $element = $browser->element($selector);
        if ($element) {
            return $element->getText();
        }
        return null;
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@address' => '.address',
            '@area' => '.advertSurface',
            '@bathrooms' => '.advertRooms',
            '@bedrooms' => '.advertBaths',
            '@description' => 'div.description p',
            '@externalFeatures' => '#tblInitialExteriores li',
            '@features2' => '.features_2 li',
            '@location' => '.breadcrumb a',
            '@garages' => '.advertGarages',
            '@internalFeatures' => '#tblInitialInteriores li',
            '@price' => '.price h2',
            '@reference' => 'h2.description span b',
            '@sectorFeatures' => '#tblInitialdelSector li',
            '@status' => '.imageContent .badge',
            '@visits' => '#numAdvertVisits',
            '@video' => 'span[onclick^=PopupVideo]',
            '@title' => 'h1',
        ];
    }
}
