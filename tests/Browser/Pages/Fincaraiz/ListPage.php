<?php

namespace Tests\Browser\Pages\Fincaraiz;

use Laravel\Dusk\Browser;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class ListPage extends Base
{
    private $page;

    public function __construct($page)
    {
        $this->page = $page;
    }
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return parent::url() . "/apartamentos/arriendo/?ad=30|$this->page||||2||8||||||||||||||||||||1|||1||griddate desc||||-1||";
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertDontSee('No Internet');
        // $browser->assertUrlIs($this->url());
    }

    public function goToDetails(Browser $browser)
    {
        $links = $this->getLinks($browser);
        if (!head($links)) {
            $missing = Cache::forever('fincaraiz.list.missing_pages', []);
            $missing[] = $this->page;
            Cache::forever('fincaraiz.list.missing_pages', $missing);
            dump('Se saltó la página: ' . $this->page);
            return false;
        }
        foreach ($links as $link) {
            $browser->visit(new PropertyPage($link))->extractData();
        }
    }

    public function getLinks(Browser $browser)
    {
        $elements = $browser->elements('@links');
        $links = [];
        foreach ($elements as $element) {
            $link = $element->getAttribute('href');
            if (!Str::contains($link, ['proyecto-nuevo', 'itemid'])) {
                $links[] = $link;
            }
        }
        return $links;
    }

    public function getNextLink(Browser $browser)
    {
        return $browser->attribute('a.nextPage', 'href');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@links' => 'ul.advert li a',
        ];
    }
}
