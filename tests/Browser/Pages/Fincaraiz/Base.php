<?php

namespace Tests\Browser\Pages\Fincaraiz;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class Base extends Page
{
    protected $source = 'Fincaraiz';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return 'https://www.fincaraiz.com.co';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertUrlIs($this->url());
    }
}
