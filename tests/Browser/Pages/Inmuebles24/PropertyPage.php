<?php

namespace Tests\Browser\Pages\Inmuebles24;

use App\Helpers;
use App\Models\{Inmuebles24Property, Property};
use Illuminate\Support\Carbon;
use Laravel\Dusk\Browser;
use Illuminate\Support\Str;

class PropertyPage extends Base
{
    /**
     * The property repository.
     *
     * @var \App\Contracts\Repository\PropertyRepositoryInterface
     */
    private $propertyRepository;

    private $url;

    public function __construct($url)
    {
        $this->url = $url;
        $this->propertyRepository = app(\App\Contracts\Repository\PropertyRepositoryInterface::class);
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs('/propiedades/*.html');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@accordions' => '[class^=AccordionItem]',
            '@address' => '.section-location b',
            '@administration-fee' => '.block-expensas span',
            '@city' => '.bread-item:nth-child(5) a span',
            '@features' => '#reactGeneralFeatures ul li h4',
            '@highlighted' => '.highlight-tag',
            '@map' => '#static-map',
            '@operations' => '.price-operation',
            '@prices' => '.price-items span',
            '@region' => '.bread-item:nth-child(4) a span',
            '@rent-type' => '[style="box-sizing: border-box; width:50%; display:inline-block; vertical-align:top; padding-top:10px; padding-bottom:10px; border-top: 1px solid #cecece; border-right: 1px solid #cecece;"]',
            '@title' => 'h1',
            '@type' => '.bread-item:nth-child(2) a span',
        ];
    }

    public function extractData(Browser $browser)
    {
        if ($this->textOrNull($browser, '.bread-item:nth-child(2) a span') === Inmuebles24Property::DEVELOPMENT) {
            return false;
        }
        $inmuebles24Property = new Inmuebles24Property($this->getAttributes($browser));
        if ($this->propertyRepository->existsByReference($inmuebles24Property->getReference())) {
            return false;
        }
        $property = new Property($inmuebles24Property->getPropertyAttributes());
        $exists = $this->propertyRepository->existsByReference($inmuebles24Property->getReference());
        if (!$exists && $this->validateAndReport($property, $inmuebles24Property)) {
            $property->save();
        }
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return $this->url;
    }

    private function getAddress(Browser $browser)
    {
        return $this->textOrNull($browser, '@address');
    }

    private function getAdministrationFee(Browser $browser)
    {
        return $this->getPriceValue($this->textOrNull($browser, '@administration-fee'));
    }

    private function getAreaValue($area)
    {
        preg_match('/\d+[,|.]?\d*[^\D]*/', $area, $matches);
        $value = head($matches);
        if ($value === false) {
            return null;
        }
        return $value;
    }

    private function getAreaUnit($area)
    {
        preg_match('/[^\d,.]+.+/', $area, $matches);
        $unit = head($matches);
        if ($unit === false) {
            return null;
        }
        return trim($unit);
    }

    private function getAttributes(Browser $browser)
    {
        return [
            Inmuebles24Property::ADDRESS => $this->getAddress($browser),
            Inmuebles24Property::ADMINISTRATION_FEE => $this->getAdministrationFee($browser),
            Inmuebles24Property::AREA => $this->getTerrainAreaValue($browser),
            Inmuebles24Property::AREA_UNIT => $this->getTerrainAreaUnit($browser),
            Inmuebles24Property::BATHROOMS => $this->getBathrooms($browser),
            Inmuebles24Property::BEDROOMS => $this->getBedrooms($browser),
            Inmuebles24Property::BUILT_AREA => $this->getBuiltAreaValue($browser),
            Inmuebles24Property::BUILT_AREA_UNIT => $this->getBuiltAreaUnit($browser),
            Inmuebles24Property::BUILT_DATE => $this->getBuiltDate($browser),
            Inmuebles24Property::CITY => $this->getCity($browser),
            Inmuebles24Property::CONDITION => $this->getCondition($browser),
            Inmuebles24Property::CURRENCY => $this->getCurrency($browser),
            Inmuebles24Property::FEATURES => $this->getFeatures($browser),
            Inmuebles24Property::GARAGES => $this->getGarages($browser),
            Inmuebles24Property::MAP => $this->getMap($browser),
            Inmuebles24Property::MAP_PUBLISH_OPTIONS => $this->getMapPublishOptions($browser),
            Inmuebles24Property::REFERENCE => $this->getReference($browser),
            Inmuebles24Property::REGION => $this->getRegion($browser),
            Inmuebles24Property::RENT_PRICE => $this->getRentPrice($browser),
            Inmuebles24Property::RENT_TYPE => $this->getRentType($browser),
            Inmuebles24Property::SALE_PRICE => $this->getSalePrice($browser),
            Inmuebles24Property::STATUS => $this->getStatus($browser),
            Inmuebles24Property::TITLE => $this->getTitle($browser),
            Inmuebles24Property::TRADE_TYPE => $this->getTradeType($browser),
            Inmuebles24Property::TYPE => $this->getType($browser),
            Inmuebles24Property::VIDEO => $this->getVideo($browser),
        ];
    }

    private function getBathrooms(Browser $browser)
    {
        $value = $this->getIconFeature($browser, 'Baño');
        return $value ? Helpers::extractNumbers($value) : $value;
    }

    private function getBedrooms(Browser $browser)
    {
        $value = $this->getIconFeature($browser, 'Recámara');
        return $value ? Helpers::extractNumbers($value) : $value;
    }

    private function getBuiltArea(Browser $browser)
    {
        return $this->getIconFeature($browser, ['m²', 'HA'], 'built');
    }

    private function getBuiltAreaUnit(Browser $browser)
    {
        return $this->getAreaUnit($this->getBuiltArea($browser));
    }

    private function getBuiltAreaValue(Browser $browser)
    {
        return $this->getAreaValue($this->getBuiltArea($browser));
    }

    private function getBuiltDate(Browser $browser)
    {
        $builtDate = str_replace(' Antigüedad', '', $this->getIconFeature($browser, ['Antigüedad', 'A estrenar']));
        if (in_array($builtDate, [Inmuebles24Property::CONDITION_NEW, Inmuebles24Property::CONDITION_IN_CONSTRUCTION])) {
            return null;
        }
        if ($builtDate) {
            return now()->subYears($builtDate)->year;
        }
        return null;
    }

    private function getCity(Browser $browser)
    {
        return $this->textOrNull($browser, '@city');
    }

    private function getCondition(Browser $browser)
    {
        $builtDate = str_replace(' Antigüedad', '', $this->getIconFeature($browser, ['Antigüedad', 'A estrenar']));
        if (in_array($builtDate, [Inmuebles24Property::CONDITION_NEW, Inmuebles24Property::CONDITION_IN_CONSTRUCTION])) {
            return $builtDate;
        }
        return Inmuebles24Property::CONDITION_USED;
    }

    private function getCurrency(Browser $browser)
    {
        return $this->getPriceCurrency(head($this->getPrices($browser)));
    }

    private function getFeatures(Browser $browser)
    {
        $accordions = $browser->elements('@accordions');
        foreach ($accordions as $key => $element) {
            if (!Str::contains($element->getAttribute('class'), 'open')) {
                $element->click();
                if ($key + 1 !== count($accordions)) {
                    $browser->pause(300);
                }
            }
        }
        $elements = $browser->elements('@features');
        $features = [];
        foreach ($elements as $element) {
            $features[] = $element->getText();
        }
        return $features;
    }

    private function getGarages(Browser $browser)
    {
        $value = $this->getIconFeature($browser, 'Estacionamiento');
        return $value ? Helpers::extractNumbers($value) : $value;
    }

    private function getIconFeature(Browser $browser, $text, $type = null)
    {
        foreach ($this->getIconFeatures($browser) as $iconFeature) {
            $iconText = $iconFeature['text'];
            if (Str::contains($iconText, $text)) {
                if (is_null($type) || $iconFeature['type'] === $type) {
                    return str_replace("\n\n", ' ', str_replace("\t", '', $iconText));
                }
            }
        }
    }

    private function getIconFeatures(Browser $browser)
    {
        return head($browser->script(<<<EOF
return Array.prototype.map.call(document.querySelectorAll('.icon-feature'), el => {
    let obj = {text: el.textContent.trim()};
    if (el.children[0].classList.contains('icon-stotal')) {
        obj.type = 'terrain';
    }
    if (el.children[0].classList.contains('icon-scubierta')) {
        obj.type = 'built';
    }
    return obj;
});
EOF
));
    }

    private function getPriceCurrency($price)
    {
        preg_match('/\w+/', $price, $matches);
        return head($matches);
    }

    private function getFromAvisoInfoScript(Browser $browser, $key)
    {
        return head($browser->script("return avisoInfo.$key"));
    }

    private function getMap(Browser $browser)
    {
        $element = $browser->element('@map');
        if ($element) {
            $mapUrl = $element->getAttribute('src');
            preg_match('/\d+\.\d+,-?\d+\.\d+/', $mapUrl, $matches);
            $map = head($matches);
            if ($map === false) {
                return null;
            }
            return $map;
        }
        return null;
    }

    private function getMapPublishOptions(Browser $browser)
    {
        $mapPublishOptions = $this->getFromAvisoInfoScript($browser, 'visibility');
        if ($mapPublishOptions === false) {
            return null;
        }
        return $mapPublishOptions;
    }

    private function getOperations(Browser $browser)
    {
        $elements = $browser->elements('@operations');
        $operations = [];
        foreach ($elements as $element) {
            $operations[] = $element->getText();
        }
        return $operations;
    }

    private function getPrices(Browser $browser)
    {
        $elements = $browser->elements('@prices');
        $prices = [];
        foreach ($elements as $element) {
            $prices[] = $element->getText();
        }
        return $prices;
    }

    private function getPriceValue($price)
    {
        return str_replace(['MN ', 'USD '], '', $price);
    }

    private function getReference(Browser $browser)
    {
        return $this->getFromAvisoInfoScript($browser, 'idAviso');
    }

    private function getRegion(Browser $browser)
    {
        return $this->textOrNull($browser, '@region');
    }

    private function getRentPrice(Browser $browser)
    {
        $prices = $this->getPrices($browser);
        switch ($this->getTradeType($browser)) {
            case Inmuebles24Property::RENT:
            case Inmuebles24Property::RENT_TRANSFER:
                return $this->getPriceValue(head($prices));
            case Inmuebles24Property::SALE_RENT:
            case Inmuebles24Property::SALE_RENT_TRANSFER:
                return $this->getPriceValue($prices[1]);
            case Inmuebles24Property::SALE:
            case Inmuebles24Property::TRANSFER:
            case Inmuebles24Property::SALE_TRANSFER:
                return null;
        }
        return $this->getPriceValue(head($prices));
    }

    private function getRentType(Browser $browser)
    {
        return $this->textOrNull($browser, '@rent-type');
    }

    private function getSalePrice(Browser $browser)
    {
        $prices = $this->getPrices($browser);
        switch ($this->getTradeType($browser)) {
            case Inmuebles24Property::SALE:
            case Inmuebles24Property::SALE_RENT:
            case Inmuebles24Property::SALE_TRANSFER:
            case Inmuebles24Property::SALE_RENT_TRANSFER:
            case Inmuebles24Property::TRANSFER:
                return $this->getPriceValue(head($prices));
            case Inmuebles24Property::RENT_TRANSFER:
                return $this->getPriceValue($prices[1]);
            case Inmuebles24Property::RENT:
                return null;
        }
        return $this->getPriceValue(head($prices));
    }

    private function getHightlighted(Browser $browser)
    {
        return $this->textOrNull($browser, '@highlighted');
    }

    private function getStatus(Browser $browser)
    {
        return $this->getHightlighted($browser);
    }

    private function getTerrainArea(Browser $browser)
    {
        return $this->getIconFeature($browser, ['m²', 'HA'], 'terrain');
    }

    private function getTerrainAreaUnit(Browser $browser)
    {
        return $this->getAreaUnit($this->getTerrainArea($browser));
    }

    private function getTerrainAreaValue(Browser $browser)
    {
        return $this->getAreaValue($this->getTerrainArea($browser));
    }

    private function textOrNull(Browser $browser, $selector)
    {
        $element = $browser->element($selector);
        if ($element) {
            return $element->getText();
        }
        return null;
    }

    private function getTitle(Browser $browser)
    {
        return $this->textOrNull($browser, '@title');
    }

    private function getTradeType(Browser $browser)
    {
        $tradeTypes = $this->getOperations($browser);
        return str_replace(
            [
                Inmuebles24Property::SALE . '-' . Inmuebles24Property::RENT . '-' . Inmuebles24Property::TRANSFER . '-' . Inmuebles24Property::TEMPORAL,
                Inmuebles24Property::SALE . '-' . Inmuebles24Property::TRANSFER . '-' . Inmuebles24Property::TEMPORAL,
                Inmuebles24Property::RENT . '-' . Inmuebles24Property::TRANSFER . '-' . Inmuebles24Property::TEMPORAL,
                Inmuebles24Property::SALE . '-' . Inmuebles24Property::RENT . '-' . Inmuebles24Property::TEMPORAL,
                Inmuebles24Property::TRANSFER . '-' . Inmuebles24Property::TEMPORAL,
                Inmuebles24Property::SALE . '-' . Inmuebles24Property::TEMPORAL,
                Inmuebles24Property::RENT . '-' . Inmuebles24Property::TEMPORAL,
                Inmuebles24Property::TEMPORAL,
            ],
            [
                Inmuebles24Property::SALE_RENT_TRANSFER,
                Inmuebles24Property::SALE_RENT_TRANSFER,
                Inmuebles24Property::RENT_TRANSFER,
                Inmuebles24Property::SALE_RENT,
                Inmuebles24Property::RENT_TRANSFER,
                Inmuebles24Property::SALE_RENT,
                Inmuebles24Property::RENT,
                Inmuebles24Property::RENT,
            ],
            join('-', $tradeTypes)
        );
    }

    private function getType(Browser $browser)
    {
        return $this->textOrNull($browser, '@type');
    }

    private function getVideo(Browser $browser)
    {
        $videos = $this->getFromAvisoInfoScript($browser, 'videos');
        if (!$videos) {
            return null;
        }
        $video = head($videos);
        if (!$video) {
            return null;
        }
        $videoId = $video['reference'];
        if (!$videoId) {
            return null;
        }
        $host = 'https://www.youtube.com/watch?v=';
        return "$host$videoId";
    }

    private function validateAndReport(Property $property, Inmuebles24Property $inmuebles24Property)
    {
        $valid = true;
        $propertyJson = $property->toJson(JSON_PRETTY_PRINT);
        $inmuebles24PropertyJson = $inmuebles24Property->toJson(JSON_PRETTY_PRINT);
        if (!$property->getRegionId()) {
            $valid = false;
            \App\Events\RegionNotFound::dispatch($propertyJson, $inmuebles24PropertyJson);
        }
        if (!$property->getCityId()) {
            $valid = false;
            \App\Events\CityNotFound::dispatch($propertyJson, $inmuebles24PropertyJson);
        }
        return $valid;
    }
}
