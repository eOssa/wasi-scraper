<?php

namespace Tests\Browser\Pages\Inmuebles24;

use App\Helpers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Laravel\Dusk\Browser;

class ListPage extends Base
{
    private $filters;
    private $label;
    private $page;
    private $propertyType;
    private $tradeType;

    private $blacklist = [
        56491297,
        56536480,
        58991533,
        58333042,
        57580165,
        59441928,
        59406594,
        56533131,
        58991451,
        59504992,
        57595876,
        57640644,
        58748202,
        57125122,
        58436288,
        59575000,
        59370220,
        55760666,
        59550065,
        56437594,
        59541635,
        59397749,
        59612207,
        59527881,
        59549453,
        58247848,
        59607330,
        57356897,
        59473225,
        59384917,
        57886300,
        57486680,
        55969576,
    ];

    public function __construct($tradeType = null, $propertyType = null, $page = 1, $filters = [], $label = '')
    {
        $this->tradeType = $tradeType;
        $this->propertyType = $propertyType;
        $this->page = $page;
        $this->filters = $filters;
        $this->label = $label;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        $propertyType = $this->getPropertyTypeForUrl();
        $tradeType = $this->getTradeTypeForUrl();
        $page = $this->getPageForUrl();
        $filter = $this->getFilterForUrl();
        return parent::url() . "/$propertyType$tradeType$filter$page.html";
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertDontSee('Not internet');
    }

    public function scrape(Browser $browser)
    {
        Cache::forever("$this->label.total_pages", $this->getTotalPages($browser));
        $links = $this->getLinks($browser);
        if (!head($links)) {
            $cacheKey = "$this->label.missing_pages";
            $missing = Cache::get($cacheKey, []);
            $missing[] = $this->page;
            Cache::forever($cacheKey, $missing);
            dump('Se saltó la página: ' . $this->page);
            return false;
        }
        foreach ($links as $link) {
            $browser->visit(new PropertyPage($link))->extractData();
        }
    }

    private function getLinks(Browser $browser)
    {
        $elements = $browser->elements('@links');
        $links = [];
        foreach ($elements as $element) {
            $link = $element->getAttribute('href');
            if (!$this->isInBlacklist($link)) {
                $links[] = $link;
            }
        }
        return $links;
    }

    private function isInBlacklist($link)
    {
        return Str::endsWith($link, $this->getBlacklist());
    }

    private function getBlacklist()
    {
        return array_map(function ($id) {
            return "$id.html";
        }, $this->blacklist);
    }

    private function getTotalPages(Browser $browser)
    {
        $total = $browser->waitFor('@totalPages')->waitFor('@totalPages')->text('@totalPages');
        return ceil(Helpers::extractNumbers(head(explode(' ', $total))) / 24);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@links' => 'h2.postingCardTitle a',
            '@totalPages' => '.list-result-title'
        ];
    }

    private function getFilterForUrl()
    {
        if (!$this->filters) {
            return '';
        }
        $filters = '';
        if (isset($this->filters['price_from'])) {
            if (isset($this->filters['price_to'])) {
                $filters = "de-{$this->filters['price_from']}-a-{$this->filters['price_to']}";
            } else {
                $filters = "mas-de-{$this->filters['price_from']}";
            }
        } else {
            $filters = "hasta-{$this->filters['price_to']}";
        }
        return $filters ? "-$filters-pesos" : '';
    }

    private function getPageForUrl()
    {
        return is_numeric($this->page) && $this->page > 1 ? "-pagina-$this->page" : '';
    }

    private function getPropertyTypeForUrl()
    {
        $propertyTypes = [
            self::APARTMENT,
            self::BUILDING,
            self::COMMERCIAL_PROPERTY,
            self::COMMERCIAL_PROPERTY_IN_MALL,
            self::COMMERCIAL_TERRAIN,
            self::COMMERCIAL_WAREHOUSE,
            self::DUPLEX,
            self::ESTATE,
            self::HOUSE,
            self::HOUSE_IN_CONDOMINIUM,
            self::HOUSE_USE_OF_FLOOR,
            self::INDUSTRIAL_TERRAIN,
            self::INDUSTRIAL_WAREHOUSE,
            self::OFFICE,
            self::PRODUCTIVE_URBAN_PROPERTY,
            self::QUINTA,
            self::RANCH,
            self::SHARED_APARTMENT,
            self::TERRAIN,
            self::VILLAGE,
        ];
        return in_array($this->propertyType, $propertyTypes) ? $this->propertyType : self::PROPERTY_TYPE;
    }

    private function getTradeTypeForUrl()
    {
        $tradeTypes = [self::SALE, self::RENT, self::TEMPORAL, self::TRANSFER];
        return in_array($this->tradeType, $tradeTypes) ? "-en-$this->tradeType" : '';
    }
}
