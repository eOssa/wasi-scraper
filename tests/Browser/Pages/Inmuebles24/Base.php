<?php

namespace Tests\Browser\Pages\Inmuebles24;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class Base extends Page
{
    const RENT = 'renta';
    const SALE = 'venta';
    const TEMPORAL = 'temporal-vacacional';
    const TRANSFER = 'traspaso';
    const APARTMENT = 'departamentos';
    const BUILDING = 'edificio';
    const COMMERCIAL_PROPERTY = 'locales-comerciales';
    const COMMERCIAL_PROPERTY_IN_MALL = 'local-en-centro-comercial';
    const COMMERCIAL_TERRAIN = 'terreno-comercial';
    const COMMERCIAL_WAREHOUSE = 'bodegas-comerciales';
    const DUPLEX = 'duplex';
    const ESTATE = 'huerta';
    const HOUSE = 'casas';
    const HOUSE_IN_CONDOMINIUM = 'casa-en-condominio';
    const HOUSE_USE_OF_FLOOR = 'casa-uso-de-suelo';
    const INDUSTRIAL_TERRAIN = 'terreno-industrial';
    const INDUSTRIAL_WAREHOUSE = 'nave-industrial';
    const OFFICE = 'oficinas';
    const PRODUCTIVE_URBAN_PROPERTY = 'inmueble-productivo-urbano';
    const PROPERTY_TYPE = 'inmuebles';
    const QUINTA = 'quinta';
    const RANCH = 'rancho';
    const SHARED_APARTMENT = 'departamento-compartido';
    const TERRAIN = 'terrenos';
    const VILLAGE = 'villa';

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return 'https://www.inmuebles24.com';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertUrlIs($this->url());
    }
}
