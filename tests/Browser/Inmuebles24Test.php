<?php

namespace Tests\Browser;
use Tests\Browser\Pages\Inmuebles24\ListPage;
use Tests\DuskTestCase;

class Inmuebles24Test extends DuskTestCase
{
    use ScrapesPages;

    const TARGET = 'inmuebles24';

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::APARTMENT, 861, ['price_to' => 1.5e6]);
    }

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment2()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::APARTMENT,
            836,
            ['price_from' => 1.5e6, 'price_to' => 2.5e6]
        );
    }

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment3()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::APARTMENT,
            783,
            ['price_from' => 2.5e6, 'price_to' => 3.5e6]
        );
    }

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment4()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::APARTMENT,
            689,
            ['price_from' => 3.5e6, 'price_to' => 4.5e6]
        );
    }

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment5()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::APARTMENT,
            512,
            ['price_from' => 4.5e6, 'price_to' => 5.5e6]
        );
    }

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment6()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::APARTMENT,
            378,
            ['price_from' => 5.5e6, 'price_to' => 6.5e6]
        );
    }

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment7()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::APARTMENT,
            737,
            ['price_from' => 6.5e6, 'price_to' => 10.5e6]
        );
    }

    /**
     * @group apartment
     * @group apartment-sale
     */
    public function testApartment8()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::APARTMENT, 816, ['price_from' => 10.5e6]);
    }

    /**
     * @group apartment
     * @group apartment-rent
     */
    public function testApartmentRent()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::APARTMENT, 768, ['price_to' => 15e3]);
    }

    /**
     * @group apartment
     * @group apartment-rent
     */
    public function testApartmentRent2()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::RENT,
            ListPage::APARTMENT,
            480,
            ['price_from' => 15e3, 'price_to' => 20e3]
        );
    }

    /**
     * @group apartment
     * @group apartment-rent
     */
    public function testApartmentRent3()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::RENT,
            ListPage::APARTMENT,
            625,
            ['price_from' => 20e3, 'price_to' => 30e3]
        );
    }

    /**
     * @group apartment
     * @group apartment-rent
     */
    public function testApartmentRent4()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::APARTMENT, 594, ['price_from' => 30e3]);
    }

    /**
     * @group apartment
     * @group apartment-temporal
     */
    public function testApartmentTemporal()
    {
        $this->makeRequest(self::TARGET, ListPage::TEMPORAL, ListPage::APARTMENT, 36);
    }

    /**
     * @group apartment
     * @group apartment-transfer
     */
    public function testApartmentTransfer()
    {
        $this->makeRequest(self::TARGET, ListPage::TRANSFER, ListPage::APARTMENT, 1);
    }

    /**
     * @group building
     */
    public function testBuilding()
    {
        $this->makeRequest(self::TARGET, null, ListPage::BUILDING, 221);
    }

    /**
     * @group commercial-property
     * @group commercial-property-sale
     */
    public function testCommercialPropertySale()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::COMMERCIAL_PROPERTY, 246);
    }

    /**
     * @group commercial-property
     * @group commercial-property-rent
     */
    public function testCommercialPropertyRent()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::COMMERCIAL_PROPERTY, 766);
    }

    /**
     * @group commercial-property
     * @group commercial-property-temporal
     */
    public function testCommercialPropertyTemporal()
    {
        $this->makeRequest(self::TARGET, ListPage::TEMPORAL, ListPage::COMMERCIAL_PROPERTY, 1);
    }

    /**
     * @group commercial-property
     * @group commercial-property-transfer
     */
    public function testCommercialPropertyTransfer()
    {
        $this->makeRequest(self::TARGET, ListPage::TRANSFER, ListPage::COMMERCIAL_PROPERTY, 5);
    }

    /**
     * @group commercial-property-in-mall
     */
    public function testCommercialPropertyInMall()
    {
        $this->makeRequest(self::TARGET, null, ListPage::COMMERCIAL_PROPERTY_IN_MALL, 156);
    }

    /**
     * @group commercial-terrain
     */
    public function testCommercialTerrain()
    {
        $this->makeRequest(self::TARGET, null, ListPage::COMMERCIAL_TERRAIN, 208);
    }

    /**
     * @group commercial-warehouse
     */
    public function testCommercialWarehouse()
    {
        $this->makeRequest(self::TARGET, null, ListPage::COMMERCIAL_WAREHOUSE, 642);
    }

    /**
     * @group duplex
     */
    public function testDuplex()
    {
        $this->makeRequest(self::TARGET, null, ListPage::DUPLEX, 30);
    }

    /**
     * @group estate
     */
    public function testEstate()
    {
        $this->makeRequest(self::TARGET, null, ListPage::ESTATE, 5);
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale1()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::HOUSE, 515, ['price_to' => 800e3]);
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale2()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            693,
            ['price_from' => 800e3, 'price_to' => 1.3e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale3()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            768,
            ['price_from' => 1.3e6, 'price_to' => 1.8e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale4()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            611,
            ['price_from' => 1.8e6, 'price_to' => 2.2e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale5()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            634,
            ['price_from' => 2.2e6, 'price_to' => 2.6e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale6()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            741,
            ['price_from' => 2.6e6, 'price_to' => 3.2e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale7()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            634,
            ['price_from' => 3.2e6, 'price_to' => 3.8e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale8()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            702,
            ['price_from' => 3.8e6, 'price_to' => 4.8e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale9()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            450,
            ['price_from' => 4.8e6, 'price_to' => 5.8e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale10()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            587,
            ['price_from' => 5.8e6, 'price_to' => 7.8e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale11()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            498,
            ['price_from' => 7.8e6, 'price_to' => 10.8e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale12()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::HOUSE,
            446,
            ['price_from' => 10.8e6, 'price_to' => 15.8e6]
        );
    }

    /**
     * @group house
     * @group house-sale
     */
    public function testHouseSale13()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::HOUSE, 681, ['price_from' => 15.8e6]);
    }

    /**
     * @group house
     * @group house-rent
     */
    public function testHouseRent()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::HOUSE, 715, ['price_to' => 30e3]);
    }

    /**
     * @group house
     * @group house-rent
     */
    public function testHouseRent2()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::HOUSE, 243, ['price_from' => 30e3]);
    }

    /**
     * @group house
     * @group house-temporal
     */
    public function testHouseTemporal()
    {
        $this->makeRequest(self::TARGET, ListPage::TEMPORAL, ListPage::HOUSE, 25);
    }

    /**
     * @group house
     * @group house-transfer
     */
    public function testHouseTransfer()
    {
        $this->makeRequest(self::TARGET, ListPage::TRANSFER, ListPage::HOUSE, 1);
    }

    /**
     * @group house-in-condominium
     * @group house-in-condominium-sale
     */
    public function testHouseInCondominiumSale()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::HOUSE_IN_CONDOMINIUM, 605, ['price_to' => 5e6]);
    }
    /**
     * @group house-in-condominium
     * @group house-in-condominium-sale
     */
    public function testHouseInCondominiumSale2()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::HOUSE_IN_CONDOMINIUM, 605, ['price_from' => 5e6]);
    }

    /**
     * @group house-in-condominium
     * @group house-in-condominium-rent
     */
    public function testHouseInCondominiumRent()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::HOUSE_IN_CONDOMINIUM, 163);
    }

    /**
     * @group house-in-condominium
     * @group house-in-condominium-temporal
     */
    public function testHouseInCondominiumTemporal()
    {
        $this->makeRequest(self::TARGET, ListPage::TEMPORAL, ListPage::HOUSE_IN_CONDOMINIUM, 5);
    }

    /**
     * @group house-in-condominium
     * @group house-in-condominium-transfer
     */
    public function testHouseInCondominiumTransfer()
    {
        $this->makeRequest(self::TARGET, ListPage::TRANSFER, ListPage::HOUSE_IN_CONDOMINIUM, 1);
    }

    /**
     * @group house-use-of-floor
     */
    public function testHouseUseOfFloor()
    {
        $this->makeRequest(self::TARGET, null, ListPage::HOUSE_USE_OF_FLOOR, 70);
    }

    /**
     * @group industrial-terrain
     */
    public function testIndustrialTerrain()
    {
        $this->makeRequest(self::TARGET, null, ListPage::INDUSTRIAL_TERRAIN, 74);
    }

    /**
     * @group industrial-warehouse
     */
    public function testIndustrialWarehouse()
    {
        $this->makeRequest(self::TARGET, null, ListPage::INDUSTRIAL_WAREHOUSE, 165);
    }

    /**
     * @group office
     * @group office-sale
     */
    public function testOfficeSale()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::OFFICE, 214);
    }

    /**
     * @group office
     * @group office-rent
     */
    public function testOfficeRent()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::OFFICE, 690, ['price_to' => 16e3]);
    }
    /**
     * @group office
     * @group office-rent
     */
    public function testOfficeRent2()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::OFFICE, 768, ['price_from' => 16e3]);
    }

    /**
     * @group office
     * @group office-temporal
     */
    public function testOfficeTemporal()
    {
        $this->makeRequest(self::TARGET, ListPage::TEMPORAL, ListPage::OFFICE, 20);
    }

    /**
     * @group office
     * @group office-transfer
     */
    public function testOfficeTransfer()
    {
        $this->markTestSkipped('En este momento no hay inmuebles como el que buscas');
        $this->makeRequest(self::TARGET, ListPage::TRANSFER, ListPage::OFFICE, 1);
    }

    /**
     * @group productive-urban-property
     */
    public function testProductiveUrbanProperty()
    {
        $this->makeRequest(self::TARGET, null, ListPage::PRODUCTIVE_URBAN_PROPERTY, 24);
    }

    /**
     * @group quinta
     */
    public function testQuinta()
    {
        $this->makeRequest(self::TARGET, null, ListPage::QUINTA, 70);
    }

    /**
     * @group ranch
     */
    public function testRanch()
    {
        $this->makeRequest(self::TARGET, null, ListPage::RANCH, 70);
    }

    /**
     * @group shared-apartment
     */
    public function testSharedApartment()
    {
        $this->makeRequest(self::TARGET, null, ListPage::SHARED_APARTMENT, 17);
    }

    /**
     * @group terrain
     * @group terrain-sale
     * @group terrain-sale-1
     */
    public function testTerrainSale()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::TERRAIN, 762, ['price_to' => 800e3]);
    }

    /**
     * @group terrain
     * @group terrain-sale
     * @group terrain-sale-2
     */
    public function testTerrainSale2()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::TERRAIN,
            613,
            ['price_from' => 800e3, 'price_to' => 2e6]
        );
    }

    /**
     * @group terrain
     * @group terrain-sale
     * @group terrain-sale-3
     */
    public function testTerrainSale3()
    {
        $this->makeRequest(
            self::TARGET,
            ListPage::SALE,
            ListPage::TERRAIN,
            731,
            ['price_from' => 2e6, 'price_to' => 7e6]
        );
    }

    /**
     * @group terrain
     * @group terrain-sale
     * @group terrain-sale-4
     */
    public function testTerrainSale4()
    {
        $this->makeRequest(self::TARGET, ListPage::SALE, ListPage::TERRAIN, 794, ['price_from' => 7e6]);
    }

    /**
     * @group terrain
     * @group terrain-rent
     */
    public function testTerrainRent()
    {
        $this->makeRequest(self::TARGET, ListPage::RENT, ListPage::TERRAIN, 136);
    }

    /**
     * @group terrain
     * @group terrain-temporal
     */
    public function testTerrainTemporal()
    {
        $this->makeRequest(self::TARGET, ListPage::TEMPORAL, ListPage::TERRAIN, 2);
    }

    /**
     * @group terrain
     * @group terrain-transfer
     */
    public function testTerrainTransfer()
    {
        $this->makeRequest(self::TARGET, ListPage::TRANSFER, ListPage::TERRAIN, 1);
    }

    /**
     * @group village
     */
    public function testVillage()
    {
        $this->makeRequest(self::TARGET, null, ListPage::VILLAGE, 34);
    }

    protected function getListInstance($tradeType = null, $propertyType = null, $page = 1, $filters = [], $label = '')
    {
        return new ListPage($tradeType, $propertyType, $page, $filters, $label);
    }
}
