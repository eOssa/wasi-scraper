<?php

namespace Tests\Browser;

use Illuminate\Support\Facades\Cache;
use Laravel\Dusk\Browser;

trait ScrapesPages
{
    protected function makeRequest($target, $tradeType = null, $propertyType = null, $totalPages = 1, $filters = [])
    {
        $this->browse(function (Browser $browser) use ($target, $tradeType, $propertyType, $totalPages, $filters) {
            $label = $this->getLabel($target, $tradeType, $propertyType, $filters);
            dump('[' . now() . "] Scraping to $label started");
            $currentPage = Cache::get("$label.page", 1);
            $total = Cache::get("$label.total_pages", $totalPages);
            if ($total + 1 <= $currentPage) {
                dump('[' . now() . "] Scraping to $label finished");
                return false;
            }
            foreach (range($currentPage, $total) as $page) {
                Cache::forever("$label.page", $page);
                $percent = round($page / $total * 100, 2);
                dump('[' . now() . "] $page / $total ($percent%)");
                $browser->visit($this->getListInstance($tradeType, $propertyType, $page, $filters, $label))->scrape();
            }
            Cache::forever("$label.page", $total + 1);
            dump('[' . now() . "] Scraping to $label finished");
        });
    }

    private function getLabel($target, $tradeType = null, $propertyType = null, $filters = [])
    {
        $tradeType = $tradeType ? "-$tradeType" : '';
        $propertyType = $propertyType ? "-$propertyType" : '';
        $filters = $filters ? '-' . json_encode($filters) : '';
        return $target . $tradeType . $propertyType . $filters;
    }

    protected function getListInstance($tradeType = null, $propertyType = null, $page = 1, $filters = [])
    {
        return new self($tradeType, $propertyType, $page, $filters);
    }
}
