<?php

namespace App;

class Helpers
{
    /**
     * Extract the number in a string.
     *
     * @param mixed $string
     *
     * @return int|null
     */
    public static function extractNumbers($string)
    {
        if (in_array(gettype($string), ['string', 'integer'])) {
            return (int) preg_replace('/[^0-9]/', '', $string);
        }
        return $string;
    }
}
