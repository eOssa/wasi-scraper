<?php

namespace App\Listeners;

use App\Events\CityNotFound;
use App\Events\RegionNotFound;
use App\Mail\ReportError;
use Illuminate\Support\Facades\Mail;

class ReportValidationFailedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegionNotFound|CityNotFound  $event
     * @return void
     */
    public function handle($event)
    {
        $message = <<<EOF
$event->message

Property: $event->propertyJson

External: $event->externalJson
EOF;
        Mail::raw($message, function (\Illuminate\Mail\Message $message) use ($event) {
            $message->subject($event->subject)
                ->to('eossa93@hotmail.com', 'Elkin Ossa')
                ->from(config('mail.from.address'), config('mail.from.name'));
        });
    }
}
