<?php

namespace App\Repositories;

use App\Contracts\Repository\Inmuebles24RegionRepositoryInterface;
use App\Models\Inmuebles24Region;

class Inmuebles24RegionRepository implements Inmuebles24RegionRepositoryInterface
{
    /**
     * The model.
     *
     * @var \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|\App\Models\Inmuebles24Region
     */
    protected $model;

    public function __construct(Inmuebles24Region $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function getWasiRegionId($regionName)
    {
        return $this->model->where(Inmuebles24Region::NAME, 'like', $regionName)->value($this->model->getKeyName());
    }
}
