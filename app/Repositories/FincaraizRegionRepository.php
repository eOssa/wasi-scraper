<?php

namespace App\Repositories;

use App\Contracts\Repository\FincaraizRegionRepositoryInterface;
use Illuminate\Support\Facades\DB;

class FincaraizRegionRepository implements FincaraizRegionRepositoryInterface
{
    /**
     * The model.
     *
     * @var \Illuminate\Database\Query\Builder
     */
    protected $model;

    public function __construct()
    {
        $this->model = DB::connection('wasi')->table('fr_dptos');
    }

    /**
     * @inheritDoc
     */
    public function getWasiRegionId($regionName)
    {
        return $this->model->where('dpto', $regionName)->value('id_dpto_inmored');
    }
}
