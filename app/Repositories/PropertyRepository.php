<?php

namespace App\Repositories;

use App\Contracts\Repository\PropertyRepositoryInterface;
use App\Models\{Property, PropertyExtraData};
use Facades\App\Models\{Property as PropertyFacade, PropertyExtraData as PropertyExtraDataFacade};

class PropertyRepository implements PropertyRepositoryInterface
{
    /**
     * The model.
     *
     * @var \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\App\Models\Property
     */
    protected $model;

    public function __construct(Property $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function existsByReference($reference)
    {
        return $this->model->where(Property::REFERENCE, $reference)->exists();
    }
}
