<?php

namespace App\Repositories;

use App\Contracts\Repository\FincaraizCityRepositoryInterface;
use Illuminate\Support\Facades\DB;

class FincaraizCityRepository implements FincaraizCityRepositoryInterface
{
    /**
     * The model.
     *
     * @var \Illuminate\Database\Query\Builder
     */
    protected $model;

    public function __construct()
    {
        $this->model = DB::connection('wasi')->table('fr_ciudades');
    }

    /**
     * @inheritDoc
     */
    public function getWasiCityId($cityName)
    {
        return $this->model->where('ciudad', $cityName)->value('id_ciudad_inmored');
    }
}
