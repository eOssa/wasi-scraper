<?php

namespace App\Repositories;

use App\Contracts\Repository\Inmuebles24CityRepositoryInterface;
use App\Models\Inmuebles24City;
use App\Models\Inmuebles24Property;

class Inmuebles24CityRepository implements Inmuebles24CityRepositoryInterface
{
    /**
     * The model.
     *
     * @var \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|\App\Models\Inmuebles24City
     */
    protected $model;

    public function __construct(Inmuebles24City $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function getWasiCityId($cityName, $regionId)
    {
        return $this->model
            ->where(Inmuebles24City::REGION_ID, $regionId)
            ->where(Inmuebles24City::NAME, 'like', $cityName)
            ->value($this->model->getKeyName());
    }
}
