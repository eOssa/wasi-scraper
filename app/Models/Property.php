<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Property extends Model
{
    const ID = 'id_bien';
    const TRADE_TYPE_ID = 'id_tipo_negocio';
    const TYPE_ID = 'id_tipo_inmueble';
    const COUNTRY_ID = 'id_pais';
    const REGION_ID = 'id_dpto';
    const CITY_ID = 'id_ciudad';
    const ZONE_ID = 'id_zona';
    const CURRENCY_ID = 'id_tipo_moneda';
    const FEATURES = 'features';
    const TITLE = 'titulo';
    const ADDRESS = 'direccion';
    const AREA = 'area';
    const AREA_UNIT = 'area_unidad';
    const BUILT_AREA = 'area_construida';
    const BUILT_AREA_UNIT = 'area_construida_unidad';
    const ADMINISTRATION_FEE = 'admon';
    const SALE_PRICE = 'precio_venta_wasi';
    const RENT_PRICE = 'precio_renta';
    const BEDROOMS = 'alcobas';
    const BATHROOMS = 'banos';
    const TYPE = 'tipo';
    const GARAGES = 'garaje';
    const RATING = 'estrato';
    const FLOOR = 'nivel';
    const FURNISHED = 'amoblado';
    const VIDEO = 'video';
    const CONDITION = 'tipo_estado';
    const STATUS = 'estado';
    const MAP = 'mapa';
    const MAP_PUBLISH_OPTION = 'mapa_publicar';
    const DATE = 'fecha';
    const BUILT_DATE = 'fecha_construccion';
    const SHARED_IN_NETWORK = 'enred';
    const VISITS = 'visitas';
    const CREATED_AT = 'fecha_registro';
    const UPDATED_AT = 'fecha_modificacion';
    const REFERENCE = 'referencia';
    const RENT_TYPE = 'tipo_renta';
    const ZIP_CODE = 'codigo_postal';
    const AVAILABILITY = 'disponibilidad';
    const COMMISSION_TYPE = 'commission_type';
    const COMMISSION_VALUE = 'commission_value';
    const LOCATION_ID = 'location_id';
    const PRIVATE_AREA = 'area_privada';
    const PRIVATE_AREA_UNIT = 'area_privada_unidad';
    const MAX_DATE = 'max(bdl_fecha)';
    const SOLD_PRICE = 'precio_vendido';
    const LOG_SALE_PRICE = 'precio_venta_log';

    const AVAILABILITY_AVAILABLE = 'Disponible';

    const CONDITION_IN_CONSTRUCTION = 'En Construccion';
    const CONDITION_NEW = 'Nuevo';
    const CONDITION_USED = 'Usado';

    const MAP_PUBLISH_OPTIONS_PUBLISH_EXACT_POINT = 'Publicar punto exacto';
    const MAP_PUBLISH_OPTIONS_PUBLISH_ZONE = 'Publicar zona';
    const MAP_PUBLISH_OPTIONS_DO_NOT_PUBLISH = 'No publicar';

    const RATING_RURAL = 'Rural';
    const RATING_COMMERCIAL = 'Comercial';

    const RENT_TYPE_DAILY = 'Diario';
    const RENT_TYPE_MONTHLY = 'Mensual';

    const STATUS_ACTIVE = 'Activo';
    const STATUS_FEATURED = 'Destacado';

    const UNIT_M2 = 'M2';
    const UNIT_BLOCKS = 'Cuadras';
    const UNIT_HECTARES = 'Hectareas';

    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function getAddress()
    {
        return $this->getAttribute(self::ADDRESS);
    }

    public function getAdministrationFee()
    {
        return $this->getAttribute(self::ADMINISTRATION_FEE);
    }

    public function getArea()
    {
        return $this->getAttribute(self::AREA);
    }

    public function getAreaUnit()
    {
        return $this->getAttribute(self::AREA_UNIT);
    }

    public function getAvailability()
    {
        return $this->getAttribute(self::AVAILABILITY);
    }

    public function getBathrooms()
    {
        return $this->getAttribute(self::BATHROOMS);
    }

    public function getBedrooms()
    {
        return $this->getAttribute(self::BEDROOMS);
    }

    public function getBuiltArea()
    {
        return $this->getAttribute(self::BUILT_AREA);
    }

    public function getBuiltAreaUnit()
    {
        return $this->getAttribute(self::BUILT_AREA_UNIT);
    }

    public function getBuiltDate()
    {
        return $this->getAttribute(self::BUILT_DATE);
    }

    public function getCityId()
    {
        return $this->getAttribute(self::CITY_ID);
    }

    public function getCommissionType()
    {
        return $this->getAttribute(self::COMMISSION_TYPE);
    }

    public function getCommissionValue()
    {
        return $this->getAttribute(self::COMMISSION_VALUE);
    }

    public function getCondition()
    {
        return $this->getAttribute(self::CONDITION);
    }

    public function getCountryId()
    {
        return $this->getAttribute(self::COUNTRY_ID);
    }

    public function getCreatedAt()
    {
        return $this->getAttribute(self::CREATED_AT);
    }

    public function getCurrencyId()
    {
        return $this->getAttribute(self::CURRENCY_ID);
    }

    public function getDate()
    {
        return $this->getAttribute(self::DATE);
    }

    public function getFloor()
    {
        return $this->getAttribute(self::FLOOR);
    }

    public function getFurnished()
    {
        return $this->getAttribute(self::FURNISHED);
    }

    public function getGarages()
    {
        return $this->getAttribute(self::GARAGES);
    }

    public function getLocationId()
    {
        return $this->getAttribute(self::LOCATION_ID);
    }

    public function getLogSalePrice()
    {
        return $this->getAttribute(self::LOG_SALE_PRICE);
    }

    public function getMap()
    {
        return $this->getAttribute(self::MAP);
    }

    public function getMapPublishOption()
    {
        return $this->getAttribute(self::MAP_PUBLISH_OPTION);
    }

    public function getMaxDate()
    {
        return $this->getAttribute(self::MAX_DATE);
    }

    public function getPrivateArea()
    {
        return $this->getAttribute(self::PRIVATE_AREA);
    }

    public function getPrivateAreaUnit()
    {
        return $this->getAttribute(self::PRIVATE_AREA_UNIT);
    }

    public function getRating()
    {
        return $this->getAttribute(self::RATING);
    }

    public function getReference()
    {
        return $this->getAttribute(self::REFERENCE);
    }

    public function getRegionId()
    {
        return $this->getAttribute(self::REGION_ID);
    }

    public function getRentPrice()
    {
        return $this->getAttribute(self::RENT_PRICE);
    }

    public function getRentType()
    {
        return $this->getAttribute(self::RENT_TYPE);
    }

    public function getSalePrice()
    {
        return $this->getAttribute(self::SALE_PRICE);
    }

    public function getSharedInNetwork()
    {
        return $this->getAttribute(self::SHARED_IN_NETWORK);
    }

    public function getSoldPrice()
    {
        return $this->getAttribute(self::SOLD_PRICE);
    }

    public function getStatus()
    {
        return $this->getAttribute(self::STATUS);
    }

    public function getTitle()
    {
        return $this->getAttribute(self::TITLE);
    }

    public function getTradeTypeId()
    {
        return $this->getAttribute(self::TRADE_TYPE_ID);
    }

    public function getType()
    {
        return $this->getAttribute(self::TYPE);
    }

    public function getTypeId()
    {
        return $this->getAttribute(self::TYPE_ID);
    }

    public function getUpdatedAt()
    {
        return $this->getAttribute(self::UPDATED_AT);
    }

    public function getVideo()
    {
        return $this->getAttribute(self::VIDEO);
    }

    public function getVisits()
    {
        return $this->getAttribute(self::VISITS);
    }

    public function getZipCode()
    {
        return $this->getAttribute(self::ZIP_CODE);
    }

    public function getZoneId()
    {
        return $this->getAttribute(self::ZONE_ID);
    }

    public function setAddress($value)
    {
        $this->setAttribute(self::ADDRESS, $value);
        return $this;
    }

    public function setAdministrationFee($value)
    {
        $this->setAttribute(self::ADMINISTRATION_FEE, $value);
        return $this;
    }

    public function setArea($value)
    {
        $this->setAttribute(self::AREA, $value);
        return $this;
    }

    public function setAreaUnit($value)
    {
        $this->setAttribute(self::AREA_UNIT, $value);
        return $this;
    }

    public function setAvailability($value)
    {
        $this->setAttribute(self::AVAILABILITY, $value);
        return $this;
    }

    public function setBathrooms($value)
    {
        $this->setAttribute(self::BATHROOMS, $value);
        return $this;
    }

    public function setBedrooms($value)
    {
        $this->setAttribute(self::BEDROOMS, $value);
        return $this;
    }

    public function setBuiltArea($value)
    {
        $this->setAttribute(self::BUILT_AREA, $value);
        return $this;
    }

    public function setBuiltAreaUnit($value)
    {
        $this->setAttribute(self::BUILT_AREA_UNIT, $value);
        return $this;
    }

    public function setBuiltDate($value)
    {
        $this->setAttribute(self::BUILT_DATE, $value);
        return $this;
    }

    public function setCity($value)
    {
        $this->setAttribute(FincaraizProperty::CITY, $value);
        return $this;
    }

    public function setCityId($value)
    {
        $this->setAttribute(self::CITY_ID, $value);
        return $this;
    }

    public function setCommissionType($value)
    {
        $this->setAttribute(self::COMMISSION_TYPE, $value);
        return $this;
    }

    public function setCommissionValue($value)
    {
        $this->setAttribute(self::COMMISSION_VALUE, $value);
        return $this;
    }

    public function setCondition($value)
    {
        $this->setAttribute(self::CONDITION, $value);
        return $this;
    }

    public function setCountry($value)
    {
        $this->setAttribute(FincaraizProperty::COUNTRY, $value);
        return $this;
    }

    public function setCountryId($value)
    {
        $this->setAttribute(self::COUNTRY_ID, $value);
        return $this;
    }

    public function setCreatedAt($value)
    {
        $this->setAttribute(self::CREATED_AT, $value);
        return $this;
    }

    public function setCurrencyId($value)
    {
        $this->setAttribute(self::CURRENCY_ID, $value);
        return $this;
    }

    public function setDate($value)
    {
        $this->setAttribute(self::DATE, $value);
        return $this;
    }

    public function setFloor($value)
    {
        $this->setAttribute(self::FLOOR, $value);
        return $this;
    }

    public function setFurnished($value)
    {
        $this->setAttribute(self::FURNISHED, $value);
        return $this;
    }

    public function setGarages($value)
    {
        $this->setAttribute(self::GARAGES, $value);
        return $this;
    }

    public function setLocationId($value)
    {
        $this->setAttribute(self::LOCATION_ID, $value);
        return $this;
    }

    public function setLogSalePrice($value)
    {
        $this->setAttribute(self::LOG_SALE_PRICE, $value);
        return $this;
    }

    public function setMap($value)
    {
        $this->setAttribute(self::MAP, $value);
        return $this;
    }

    public function setMapPublishOption($value)
    {
        $this->setAttribute(self::MAP_PUBLISH_OPTION, $value);
        return $this;
    }

    public function setMaxDate($value)
    {
        $this->setAttribute(self::MAX_DATE, $value);
        return $this;
    }

    public function setPrivateArea($value)
    {
        $this->setAttribute(self::PRIVATE_AREA, $value);
        return $this;
    }

    public function setPrivateAreaUnit($value)
    {
        $this->setAttribute(self::PRIVATE_AREA_UNIT, $value);
        return $this;
    }

    public function setRating($value)
    {
        $this->setAttribute(self::RATING, $value);
        return $this;
    }

    public function setReference($value)
    {
        $this->setAttribute(self::REFERENCE, $value);
        return $this;
    }

    public function setRegion($value)
    {
        $this->setAttribute(FincaraizProperty::REGION, $value);
        return $this;
    }

    public function setRegionId($value)
    {
        $this->setAttribute(self::REGION_ID, $value);
        return $this;
    }

    public function setRentPrice($value)
    {
        $this->setAttribute(self::RENT_PRICE, $value);
        return $this;
    }

    public function setRentType($value)
    {
        $this->setAttribute(self::RENT_TYPE, $value);
        return $this;
    }

    public function setSalePrice($value)
    {
        $this->setAttribute(self::SALE_PRICE, $value);
        return $this;
    }

    public function setSharedInNetwork($value)
    {
        $this->setAttribute(self::SHARED_IN_NETWORK, $value);
        return $this;
    }

    public function setSoldPrice($value)
    {
        $this->setAttribute(self::SOLD_PRICE, $value);
        return $this;
    }

    public function setStatus($value)
    {
        $this->setAttribute(self::STATUS, $value);
        return $this;
    }

    public function setTitle($value)
    {
        $this->setAttribute(self::TITLE, $value);
        return $this;
    }

    public function setTradeTypeId($value)
    {
        $this->setAttribute(self::TRADE_TYPE_ID, $value);
        return $this;
    }

    public function setType($value)
    {
        $this->setAttribute(self::TYPE, $value);
        return $this;
    }

    public function setTypeId($value)
    {
        $this->setAttribute(self::TYPE_ID, $value);
        return $this;
    }

    public function setVideo($value)
    {
        $this->setAttribute(self::VIDEO, $value);
        return $this;
    }

    public function setVisits($value)
    {
        $this->setAttribute(self::VISITS, $value);
        return $this;
    }

    public function setZipCode($value)
    {
        $this->setAttribute(self::ZIP_CODE, $value);
        return $this;
    }

    public function setZoneId($value)
    {
        $this->setAttribute(self::ZONE_ID, $value);
        return $this;
    }
}
