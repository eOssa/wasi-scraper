<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inmuebles24Region extends Model
{
    const NAME = 'name';

    protected $connection = 'sqlite';
    protected $guarded = [];
    public $timestamps = false;
}
