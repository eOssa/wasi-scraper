<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyFeature extends Model
{
    const AIR_CONDITIONER = 1;
    const ALARM = 2;
    const FURNISHED = 4;
    const BALCONY = 5;
    const AUXILIARY_BATH = 6;
    const LIBRARY_STUDY = 7;
    const SERVICE_ROOM = 8;
    const ROOM_HALL = 9;
    const SAUNA = 10;
    const PANORAMIC_VIEW = 11;
    const HEATER = 12;
    const CHIMNEY = 13;
    const CITOPHONE = 14;
    const AMERICAN_STYLE_BAR = 15;
    const INTEGRAL_KITCHEN = 16;
    const AMERICAN_STYLE_KITCHEN = 17;
    const AUXILIARY_DINING_ROOM = 18;
    const HOUSEHOLD_GAS = 19;
    const LAUNDRY_AREA = 20;
    const DEPOSIT = 21;
    const PANTRY = 22;
    const CONDUCTORS_ROOM = 23;
    const CERAMIC_MARBLE_FLOOR = 24;
    const SQUASH_COURT = 25;
    const TENNIS_COURT = 26;
    const SPORTS_AREAS = 27;
    const GYM = 28;
    const GARDEN = 29;
    const YARD = 30;
    const GOLF_CAGE = 31;
    const POOL = 32;
    const CHILD_AREA = 33;
    const PARKLAND = 34;
    const GARAGE = 35;
    const BUSINESS_OFFICE = 36;
    const PARKING_VISITORS = 37;
    const INTERNET_ROOM = 38;
    const COMMON_ROOM = 39;
    const TERRACE = 40;
    const BIFAMILIAR_HOUSING = 41;
    const MULTIFAMILIARY_HOUSING = 42;
    const RECEPTION = 43;
    const ELEVATOR = 44;
    const CLOSED_TV_CIRCUIT = 45;
    const CLOSED_URBANIZATION = 46;
    const POWER_PLANT = 47;
    const SURVEILLANCE = 48;
    const ACCESS_PAVED = 49;
    const URBAN_AREA = 50;
    const KIOSK = 51;
    const NEARBY_RIVER = 52;
    const FRUIT_TREES = 53;
    const NATIVE_FOREST = 54;
    const PARKING_SPACE = 55;
    const BARN = 56;
    const SHED = 57;
    const GREENHOUSE = 58;
    const STABLE = 59;
    const WATER_WELL = 60;
    const IRRIGATION_SYSTEM = 61;
    const BASKETBALL_COURT = 62;
    const SOCCER_FIELD = 63;
    const CAMPING_AREA = 64;
    const ON_MAJOR_ROAD = 65;
    const COUNTRYSIDE = 66;
    const SHOPPING_AREA = 67;
    const INDUSTRIAL_ZONE = 68;
    const RESIDENTIAL_AREA = 69;
    const COLLEGES_UNIVERSITIES = 70;
    const NEAR_PUBLIC_TRANSPORTATION = 71;
    const NEAR_PARKS = 72;
    const MALLS = 73;
    const BEACHES = 75;
    const SOCIAL_AREA = 78;
    const LAKE = 79;
    const MAIN_ROOM_BATHROOM = 82;
    const BUILTIN_WARDROBES = 84;
    const EQUIPPED_KITCHEN = 85;
    const DOUBLE_WINDOW = 86;
    const MOUNTAIN = 87;
    const SOCIAL_CLUB = 88;
    const STORAGE_ROOM = 90;
    const GRILL = 92;
    const BIFAMILIAR = 93;
    const CLOSET = 98;
    const TURKISH_BATH = 99;
    const JACUZZI = 100;
}
