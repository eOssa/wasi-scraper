<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Inmuebles24Property extends Model
{
    const ADDRESS = 'address';
    const ADMINISTRATION_FEE = 'administration_fee';
    const AREA = 'area';
    const AREA_UNIT = 'area_unit';
    const BATHROOMS = 'bathrooms';
    const BEDROOMS = 'bedrooms';
    const BUILT_AREA = 'built_area';
    const BUILT_AREA_UNIT = 'built_area_unit';
    const BUILT_DATE = 'built_date';
    const CITY = 'city';
    const CONDITION = 'condition';
    const CURRENCY = 'currency';
    const FEATURES = 'features';
    const GARAGES = 'garages';
    const MAP = 'map';
    const MAP_PUBLISH_OPTIONS = 'map_publish_options';
    const REFERENCE = 'reference';
    const REGION = 'region';
    const RENT_PRICE = 'rent_price';
    const RENT_TYPE = 'rent_type';
    const SALE_PRICE = 'sale_price';
    const STATUS = 'status';
    const TITLE = 'title';
    const TRADE_TYPE = 'trade_type';
    const TYPE = 'type';
    const VIDEO = 'video';

    const SALE = 'Venta';
    const RENT = 'Renta';
    const TRANSFER = 'Traspaso';
    const SALE_RENT = self::SALE . '-' . self::RENT;
    const SALE_TRANSFER = self::SALE . '-' . self::TRANSFER;
    const RENT_TRANSFER = self::RENT . '-' . self::TRANSFER;
    const SALE_RENT_TRANSFER = self::SALE . '-' . self::RENT . '-' . self::TRANSFER;
    const TEMPORAL = 'Temporal/Vacacional';
    const DEVELOPMENT = 'Desarrollos';

    const MAP_PUBLISH_OPTIONS_EXACT = 'EXACT';
    const MAP_PUBLISH_OPTIONS_ZONE = 'ZONE';
    const MAP_PUBLISH_OPTIONS_DO_NOT_PUBLISH = 'NONE';

    const STATUS_ACTIVE = 'Activo';
    const STATUS_FEATURED = 'Destacado';
    const STATUS_SUPER_FEATURED = 'Super destacado';

    const CONDITION_NEW = 'A estrenar';
    const CONDITION_IN_CONSTRUCTION = 'En construcción';
    const CONDITION_USED = 'Usado';

    const TYPE_APARTMENT = 'Departamento';
    const TYPE_BUILDING = 'Edificio';
    const TYPE_COMMERCIAL_PROPERTY = 'Local comercial';
    const TYPE_COMMERCIAL_PROPERTY_IN_MALL = 'Local en centro comercial';
    const TYPE_COMMERCIAL_TERRAIN = 'Terreno comercial';
    const TYPE_COMMERCIAL_WAREHOUSE = 'Bodega comercial';
    const TYPE_DUPLEX = 'Dúplex';
    const TYPE_ESTATE = 'Huerta';
    const TYPE_HOUSE = 'Casa';
    const TYPE_HOUSE_IN_CONDOMINIUM = 'Casa en condominio';
    const TYPE_HOUSE_USE_OF_FLOOR = 'Casa uso de suelo';
    const TYPE_INDUSTRIAL_TERRAIN = 'Terreno industrial';
    const TYPE_INDUSTRIAL_WAREHOUSE = 'Nave industrial';
    const TYPE_OFFICE = 'Oficina';
    const TYPE_PRODUCTIVE_URBAN_PROPERTY = 'Inmueble productivo urbano';
    const TYPE_QUINTA = 'Quinta';
    const TYPE_RANCH = 'Rancho';
    const TYPE_SHARED_APARTMENT = 'Departamento Compartido';
    const TYPE_TERRAIN = 'Terreno / Lote';
    const TYPE_VILLAGE = 'Villa';

    protected $guarded = [];

    public function getAddress()
    {
        return $this->getAttribute(self::ADDRESS);
    }

    public function getAdministrationFee()
    {
        return $this->getAttribute(self::ADMINISTRATION_FEE);
    }

    public function getArea()
    {
        return $this->getAttribute(self::AREA);
    }

    public function getAreaUnit()
    {
        return $this->getAttribute(self::AREA_UNIT);
    }

    public function getBathrooms()
    {
        return $this->getAttribute(self::BATHROOMS);
    }

    public function getBedrooms()
    {
        return $this->getAttribute(self::BEDROOMS);
    }

    public function getBuiltArea()
    {
        return $this->getAttribute(self::BUILT_AREA);
    }

    public function getBuiltAreaUnit()
    {
        return $this->getAttribute(self::BUILT_AREA_UNIT);
    }

    public function getBuiltDate()
    {
        return $this->getAttribute(self::BUILT_DATE);
    }

    public function getCity()
    {
        return $this->getAttribute(self::CITY);
    }

    public function getCityId()
    {
        $city = $this->getCity();
        $regionId = $this->getRegionId();
        switch ($city) {
            case 'La Magdalena Contreras':
            case 'Cuajimalpa de Morelos':
                return 480381; // Ciudad de México
            case 'Benito Juárez':
                if ($regionId === 1755) {
                    return 477409; // Cancún
                }
                break;
            case 'Othón P. Blanco':
                if ($regionId === 1755) {
                    return 479279; // Chetumal
                }
                break;
            case 'La Paz':
                if ($regionId === 1747) {
                    return 503226; // Los Reyes
                }
                break;
            case 'Chicxulub Pueblo':
                if ($regionId === 1763) {
                    return 479464; // Chicxulub
                }
                break;
            case 'Solidaridad':
                if ($regionId === 1755) {
                    return 510630; // Playa del Carmen
                }
                break;
            case 'Otzolotepec':
                if ($regionId === 1747) {
                    return 529204; // Villa Cuauhtémoc
                }
                break;
            case 'Timilpan':
                if ($regionId === 1747) {
                    return 517361; // San Andrés Timilpan
                }
                break;
            case 'Yauhquemecan':
                if ($regionId === 1761) {
                    return 530131; // Yauhquemehcan
                }
                break;
            case 'TlaltizapAn de Zapata':
                if ($regionId === 1749) {
                    return 527121; // Tlaltizapán
                }
                break;
            case 'Florencio Villarreal':
                if ($regionId === 1744) {
                    return 482171; // Cruz Grande
                }
                break;
        }
        /** @var \App\Contracts\Repository\Inmuebles24CityRepositoryInterface $inmuebles24CityRepository */
        $inmuebles24CityRepository = app(\App\Contracts\Repository\Inmuebles24CityRepositoryInterface::class);
        return $inmuebles24CityRepository->getWasiCityId($city, $regionId) ?? 0;
    }

    public function getCondition()
    {
        return $this->getAttribute(self::CONDITION);
    }

    public function getConditionWasi()
    {
        switch ($this->getCondition()) {
            case self::CONDITION_NEW:
                return Property::CONDITION_NEW;
            case self::CONDITION_IN_CONSTRUCTION:
                return Property::CONDITION_IN_CONSTRUCTION;
            case self::CONDITION_USED:
                return Property::CONDITION_USED;
        }
    }

    public function getCreatedAt()
    {
        return $this->getAttribute(self::CREATED_AT);
    }

    public function getCurrency()
    {
        return $this->getAttribute(self::CURRENCY);
    }

    public function getCurrencyId()
    {
        $currency = $this->getCurrency();
        switch ($currency) {
            case 'MN':
                return Currency::MXN;
            case 'USD':
                return Currency::USD;
            default:
                return Currency::MXN;
        }
    }

    public function getFeatures()
    {
        return $this->getAttribute(self::FEATURES);
    }

    public function getFeaturesIds()
    {
        $features = $this->getFeatures();
        $featuresIds = [];
        $equivalences = $this->getFeaturesEquivalences();
        foreach ($features as $feature) {
            if (isset($equivalences[$feature])) {
                $featuresIds[] = $equivalences[$feature];
            }
        }
        return array_values(array_unique($featuresIds));
    }

    private function getFeaturesEquivalences()
    {
        return [
            'A orilla de calle asfaltada' => PropertyFeature::ACCESS_PAVED,
            'Acceso por calle de concreto' => PropertyFeature::ACCESS_PAVED,
            'Aire acondicionado' => PropertyFeature::AIR_CONDITIONER,
            'Aire Acondicionado Central' => PropertyFeature::AIR_CONDITIONER,
            'Alberca' => PropertyFeature::POOL,
            'Alberca al Aire Libre' => PropertyFeature::POOL,
            'Alberca Climatizada' => PropertyFeature::POOL,
            'Alberca para Niños' => PropertyFeature::POOL,
            'Amueblado' => PropertyFeature::FURNISHED,
            'árboles frutales' => PropertyFeature::FRUIT_TREES,
            'Area de lavandería' => PropertyFeature::SERVICE_ROOM,
            'área de Juegos Infantiles' => PropertyFeature::CHILD_AREA,
            'Area de servicios' => PropertyFeature::SERVICE_ROOM,
            'Area deportiva' => PropertyFeature::SPORTS_AREAS,
            'Areas verdes' => PropertyFeature::PARKLAND,
            'Armario(s)' => PropertyFeature::CLOSET,
            'Asador' => PropertyFeature::GRILL,
            'Av. acceso asfaltada' => PropertyFeature::ACCESS_PAVED,
            'Balcón(es)' => PropertyFeature::BALCONY,
            'Balcón con verja de seguridad' => PropertyFeature::BALCONY,
            'Baño de servicio' => PropertyFeature::AUXILIARY_BATH,
            'Barriada con garita de acceso' => 80,
            'Business Center' => PropertyFeature::BUSINESS_OFFICE,
            'Calentador de agua' => PropertyFeature::HEATER,
            'Campo de Golf' => PropertyFeature::GOLF_CAGE,
            'Cancha(s)' => PropertyFeature::SPORTS_AREAS,
            'Cancha de squash' => PropertyFeature::SQUASH_COURT,
            'Cancha de tenis' => PropertyFeature::TENNIS_COURT,
            'Canchas deportivas' => PropertyFeature::SPORTS_AREAS,
            'Centros Comerciales Cercanos' => PropertyFeature::MALLS,
            'Cerca a Parque (a menos de 2 cdras)' => PropertyFeature::NEAR_PARKS,
            'Cerca al Mar (a menos de 5 cdras)' => PropertyFeature::BEACHES,
            'Cerca del tránsito' => PropertyFeature::NEAR_PUBLIC_TRANSPORTATION,
            'Chimenea' => PropertyFeature::CHIMNEY,
            'Circuito Cerrado' => PropertyFeature::CLOSED_TV_CIRCUIT,
            'Citofonía' => PropertyFeature::CITOPHONE,
            'Closet' => PropertyFeature::CLOSET,
            'Closets' => PropertyFeature::CLOSET,
            'Cocina integral' => PropertyFeature::INTEGRAL_KITCHEN,
            'Control de accesos' => PropertyFeature::RECEPTION,
            'Cuartos de servicio' => PropertyFeature::SERVICE_ROOM,
            'Deposito' => PropertyFeature::DEPOSIT,
            'Elevador(es)' => PropertyFeature::ELEVATOR,
            'En condominio' => PropertyFeature::CLOSED_URBANIZATION,
            'Escuelas Cercanas' => PropertyFeature::COLLEGES_UNIVERSITIES,
            'Estacionamiento de visitas' => PropertyFeature::PARKING_VISITORS,
            'Estudio' => PropertyFeature::LIBRARY_STUDY,
            'Frente a Parque' => PropertyFeature::NEAR_PARKS,
            'Frente al mar (primera fila)' => PropertyFeature::BEACHES,
            'Frente al río' => PropertyFeature::NEARBY_RIVER,
            'Garaje techado' => PropertyFeature::GARAGE,
            'Gas' => PropertyFeature::HOUSEHOLD_GAS,
            'Gas centralizado' => PropertyFeature::HOUSEHOLD_GAS,
            'Gimnasio' => PropertyFeature::GYM,
            'Jacuzzi' => PropertyFeature::JACUZZI,
            'Jardín(es)' => PropertyFeature::GARDEN,
            'Jardín Común' => PropertyFeature::GARDEN,
            'Jardín Privado' => PropertyFeature::GARDEN,
            'Laguna' => PropertyFeature::LAKE,
            'Parque industrial cerrado' => PropertyFeature::INDUSTRIAL_ZONE,
            'Parques Cercanos' => PropertyFeature::NEAR_PARKS,
            'Parrilla' => PropertyFeature::GRILL,
            'Playa' => PropertyFeature::BEACHES,
            'Patio' => PropertyFeature::YARD,
            'Pavimento' => PropertyFeature::ACCESS_PAVED,
            'Perímetro cercado' => PropertyFeature::CLOSED_URBANIZATION,
            'Piscina cubierta' => PropertyFeature::POOL,
            'Planta de emergencia' => PropertyFeature::POWER_PLANT,
            'Portón' => PropertyFeature::RECEPTION,
            'Pozo de Agua' => PropertyFeature::WATER_WELL,
            'Quebrada' => PropertyFeature::NEARBY_RIVER,
            'Recepción' => PropertyFeature::RECEPTION,
            'Riego automático' => PropertyFeature::IRRIGATION_SYSTEM,
            'Río' => PropertyFeature::NEARBY_RIVER,
            'Sala' => PropertyFeature::COMMON_ROOM,
            'Sala comunal' => PropertyFeature::COMMON_ROOM,
            'Sala de Computos' => PropertyFeature::INTERNET_ROOM,
            'Sala de estar' => PropertyFeature::COMMON_ROOM,
            'Salón de usos múltiples' => PropertyFeature::COMMON_ROOM,
            'Sauna' => PropertyFeature::SAUNA,
            'Seguridad' => PropertyFeature::SURVEILLANCE,
            'Seguridad privada' => PropertyFeature::SURVEILLANCE,
            'Sistema de alarma de seguridad' => PropertyFeature::ALARM,
            'Televisión' => PropertyFeature::CLOSED_TV_CIRCUIT,
            'Terraza' => PropertyFeature::TERRACE,
            'Turco' => PropertyFeature::TURKISH_BATH,
            'Urbanización privada' => PropertyFeature::CLOSED_URBANIZATION,
            'Utensilios de Cocina' => PropertyFeature::EQUIPPED_KITCHEN,
            'Vista Panorámica' => PropertyFeature::PANORAMIC_VIEW,
            'Zona industrial' => PropertyFeature::INDUSTRIAL_ZONE,
            'Zona residencial' => PropertyFeature::RESIDENTIAL_AREA,
        ];
    }

    public function getGarages()
    {
        return $this->getAttribute(self::GARAGES);
    }

    public function getMap()
    {
        return $this->getAttribute(self::MAP);
    }

    public function getMapPublishOption()
    {
        switch ($this->getMapPublishOptions()) {
            case self::MAP_PUBLISH_OPTIONS_EXACT:
                return Property::MAP_PUBLISH_OPTIONS_PUBLISH_EXACT_POINT;
            case self::MAP_PUBLISH_OPTIONS_ZONE:
                return Property::MAP_PUBLISH_OPTIONS_PUBLISH_ZONE;
            case self::MAP_PUBLISH_OPTIONS_DO_NOT_PUBLISH:
                return Property::MAP_PUBLISH_OPTIONS_DO_NOT_PUBLISH;
        }
    }

    public function getMapPublishOptions()
    {
        return $this->getAttribute(self::MAP_PUBLISH_OPTIONS);
    }

    public function getPropertyAttributes()
    {
        return [
            Property::ADDRESS => $this->getAddress(),
            Property::ADMINISTRATION_FEE => $this->getAdministrationFee(),
            Property::AREA => $this->getArea(),
            Property::AREA_UNIT => $this->getAreaUnit(),
            Property::BATHROOMS => $this->getBathrooms(),
            Property::BEDROOMS => $this->getBedrooms(),
            Property::BUILT_AREA => $this->getBuiltArea(),
            Property::BUILT_AREA_UNIT => $this->getBuiltAreaUnit(),
            Property::BUILT_DATE => $this->getBuiltDate(),
            Property::CITY_ID => $this->getCityId(),
            Property::CONDITION => $this->getConditionWasi(),
            Property::CURRENCY_ID => $this->getCurrencyId(),
            Property::FEATURES => $this->getFeaturesIds(),
            Property::GARAGES => $this->getGarages(),
            Property::MAP => $this->getMap(),
            Property::MAP_PUBLISH_OPTION => $this->getMapPublishOption(),
            Property::REFERENCE => $this->getReference(),
            Property::REGION_ID => $this->getRegionId(),
            Property::RENT_PRICE => $this->getRentPrice(),
            Property::RENT_TYPE => $this->getRentTypeWasi(),
            Property::SALE_PRICE => $this->getSalePrice(),
            Property::STATUS => $this->getStatusWasi(),
            Property::TITLE => $this->getTitle(),
            Property::TRADE_TYPE_ID => $this->getTradeTypeId(),
            Property::TYPE_ID => $this->getTypeId(),
            Property::VIDEO => $this->getVideo(),
        ];
    }

    public function getReference()
    {
        return "Inmuebles24:{$this->getAttribute(self::REFERENCE)}";
    }

    public function getRegion()
    {
        return $this->getAttribute(self::REGION);
    }

    public function getRegionId()
    {
        if ($this->region_id) {
            return $this->region_id;
        }
        $region = $this->getRegion();
        /** @var \App\Contracts\Repository\Inmuebles24RegionRepositoryInterface $inmuebles24RegionRepository */
        $inmuebles24RegionRepository = app(\App\Contracts\Repository\Inmuebles24RegionRepositoryInterface::class);
        $this->region_id = $inmuebles24RegionRepository->getWasiRegionId($region) ?? 0;
        return $this->region_id;
    }

    public function getRentPrice()
    {
        return $this->getAttribute(self::RENT_PRICE);
    }

    public function getRentType()
    {
        return $this->getAttribute(self::RENT_TYPE);
    }

    public function getRentTypeWasi()
    {
        $rentType = $this->getRentType();
        if ($rentType === 'Por dia') {
            return Property::RENT_TYPE_DAILY;
        }
        return Property::RENT_TYPE_MONTHLY;
    }

    public function getSalePrice()
    {
        return $this->getAttribute(self::SALE_PRICE);
    }

    public function getStatus()
    {
        return $this->getAttribute(self::STATUS);
    }

    public function getStatusWasi()
    {
        $status = $this->getAttribute(self::STATUS);
        if (in_array($status, [self::STATUS_FEATURED, self::STATUS_SUPER_FEATURED])) {
            return Property::STATUS_FEATURED;
        }
        return Property::STATUS_ACTIVE;
    }

    public function getTitle()
    {
        return $this->getAttribute(self::TITLE);
    }

    public function getTradeType()
    {
        return $this->getAttribute(self::TRADE_TYPE);
    }

    public function getTradeTypeId()
    {
        switch ($this->getTradeType()) {
            case self::SALE:
                return TradeType::SALE;
            case self::RENT:
                return TradeType::RENT;
            case self::TRANSFER:
                return TradeType::TRANSFER;
            case self::SALE_TRANSFER:
                return TradeType::SALE_TRANSFER;
            case self::SALE_RENT:
                return TradeType::SALE_RENT;
            case self::RENT_TRANSFER:
                return TradeType::RENT_TRANSFER;
            case self::SALE_RENT_TRANSFER:
                return TradeType::SALE_RENT_TRANSFER;
        }
        return 0;
    }

    public function getType()
    {
        return $this->getAttribute(self::TYPE);
    }

    public function getTypeId()
    {
        switch ($this->getType()) {
            case self::TYPE_APARTMENT:
            case self::TYPE_SHARED_APARTMENT:
                return PropertyType::APARTMENT;
            case self::TYPE_BUILDING:
                return PropertyType::BUILDING;
            case self::TYPE_COMMERCIAL_PROPERTY:
            case self::TYPE_COMMERCIAL_PROPERTY_IN_MALL:
                return PropertyType::COMMERCIAL_PROPERTY;
            case self::TYPE_TERRAIN:
            case self::TYPE_COMMERCIAL_TERRAIN:
            case self::TYPE_INDUSTRIAL_TERRAIN:
                return PropertyType::TERRAIN;
            case self::TYPE_COMMERCIAL_WAREHOUSE:
                return PropertyType::WAREHOUSE;
            case self::TYPE_DUPLEX:
                return PropertyType::DUPLEX;
            case self::TYPE_ESTATE:
                return PropertyType::ESTATE;
            case self::TYPE_HOUSE:
            case self::TYPE_HOUSE_USE_OF_FLOOR:
                return PropertyType::HOUSE;
            case self::TYPE_HOUSE_IN_CONDOMINIUM:
                return PropertyType::CONDOMINIUM;
            case self::TYPE_INDUSTRIAL_WAREHOUSE:
                return PropertyType::INDUSTRIAL_WAREHOUSE;
            case self::TYPE_OFFICE:
                return PropertyType::OFFICE;
            case self::TYPE_QUINTA:
            case self::TYPE_VILLAGE:
                return PropertyType::QUINTAS;
            case self::TYPE_RANCH:
                return PropertyType::FARMHOUSE;
        }
        $title = mb_strtolower($this->getTitle());
        if (Str::contains($title, ['departamento', 'apartamento'])) {
            return PropertyType::APARTMENT;
        }
        if (Str::contains($title, 'hotel')) {
            return PropertyType::HOTEL;
        }
        if (Str::contains($title, 'edificio')) {
            return PropertyType::BUILDING;
        }
        if (Str::contains($title, ['local', 'clinica'])) {
            return PropertyType::COMMERCIAL_PROPERTY;
        }
        if (Str::contains($title, 'campo')) {
            return PropertyType::LAND;
        }
        if (Str::contains($title, 'casona')) {
            return PropertyType::HOSTEL;
        }
        if (Str::contains($title, 'bodega')) {
            return PropertyType::WAREHOUSE;
        }
        if (Str::contains($title, 'finca')) {
            return PropertyType::FARMHOUSE;
        }
        if (Str::contains($title, 'terreno')) {
            return PropertyType::TERRAIN;
        }
        if (Str::contains($title, ['casa', 'propiedad'])) {
            return PropertyType::HOUSE;
        }
        return 0;
    }

    public function getUpdatedAt()
    {
        return $this->getAttribute(self::UPDATED_AT);
    }

    public function getVideo()
    {
        return $this->getAttribute(self::VIDEO);
    }
}
