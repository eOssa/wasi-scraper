<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inmuebles24City extends Model
{
    const REGION_ID = 'region_id';
    const NAME = 'name';

    protected $connection = 'sqlite';
    protected $guarded = [];
    public $timestamps = false;
}
