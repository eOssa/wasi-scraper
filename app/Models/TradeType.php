<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeType extends Model
{
    const SALE = 1;
    const RENT = 2;
    const TRANSFER = 3;
    const SALE_TRANSFER = 4;
    const SALE_RENT = 5;
    const RENT_TRANSFER = 6;
    const SALE_RENT_TRANSFER = 7;
}
