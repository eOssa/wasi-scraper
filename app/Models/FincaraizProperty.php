<?php

namespace App\Models;

use App\Contracts\Repository\{FincaraizCityRepositoryInterface, FincaraizRegionRepositoryInterface};
use Illuminate\Database\Eloquent\Model;

class FincaraizProperty extends Model
{
    const ADDRESS = 'address';
    const ADMINISTRATION_FEE = 'administration_fee';
    const AREA = 'area';
    const BATHROOMS = 'bathrooms';
    const BEDROOMS = 'bedrooms';
    const BUILT_AREA = 'built_area';
    const CITY = 'city';
    const CONDITION = 'condition';
    const COUNTRY = 'country';
    const FLOOR = 'floor';
    const GARAGES = 'garages';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const NEIGHBORHOOD = 'neighborhood';
    const PRICE = 'price';
    const PRIVATE_AREA = 'private_area';
    const RATING = 'rating';
    const REFERENCE = 'reference';
    const REGION = 'region';
    const TITLE = 'title';
    const TRADE_TYPE = 'trade_type';
    const TYPE = 'type';
    const VIDEO = 'video';
    const VISITS = 'visits';
    const ZONE = 'zone';

    const TRADE_TYPE_SALE = 'Venta';
    const TRADE_TYPE_RENT = 'Arriendo';

    protected $guarded = [];
    protected $casts = [
        self::ADMINISTRATION_FEE => 'integer',
        self::CREATED_AT => 'datetime',
        self::GARAGES => 'integer',
        self::PRICE => 'integer',
        self::UPDATED_AT => 'datetime',
    ];

    public function getAddress()
    {
        return (string) $this->getAttribute(self::ADDRESS);
    }

    public function getAdministrationFee()
    {
        return (int) $this->getAttribute(self::ADMINISTRATION_FEE);
    }

    public function getArea()
    {
        return $this->getAttribute(self::AREA);
    }

    public function getAreaValue()
    {
        list($value) = explode(' ', $this->getArea());
        return (string) $value;
    }

    public function getAreaUnit()
    {
        if (!$this->getArea()) {
            return Property::UNIT_M2;
        }
        list($value, $unit) = explode(' ', $this->getArea());
        if ($unit === 'm²') {
            return Property::UNIT_M2;
        }
        if ($unit === 'Ha') {
            return Property::UNIT_HECTARES;
        }
        return ucfirst($unit);
    }

    public function getAvailability()
    {
        return Property::AVAILABILITY_AVAILABLE;
    }

    public function getBathrooms()
    {
        return (string) $this->getAttribute(self::BATHROOMS);
    }

    public function getBedrooms()
    {
        return (string) $this->getAttribute(self::BEDROOMS);
    }

    public function getBuiltArea()
    {
        return $this->getAttribute(self::BUILT_AREA);
    }

    public function getBuiltAreaValue()
    {
        list($value) = explode(' ', $this->getBuiltArea());
        return (string) $value;
    }

    public function getBuiltAreaUnit()
    {
        if (!$this->getBuiltArea()) {
            return Property::UNIT_M2;
        }
        list($value, $unit) = explode(' ', $this->getBuiltArea());
        if ($unit === 'm²') {
            return Property::UNIT_M2;
        }
        if ($unit === 'Ha') {
            return Property::UNIT_HECTARES;
        }
        return ucfirst($unit);
    }

    public function getBuiltDate()
    {
        return '';
    }

    public function getCity()
    {
        return $this->getAttribute(self::CITY);
    }

    public function getCityId()
    {
        /** @var \App\Contracts\Repository\FincaraizCityRepositoryInterface $fincaraizCityRepository */
        $fincaraizCityRepository = app(FincaraizCityRepositoryInterface::class);
        return (int) $fincaraizCityRepository->getWasiCityId($this->getCity());
    }

    public function getCommissionType()
    {
        return 'percent';
    }

    public function getCommissionValue()
    {
        return '';
    }

    public function getCondition()
    {
        return $this->getAttribute(self::CONDITION) ?: Property::CONDITION_USED;
    }

    public function getCountry()
    {
        return $this->getAttribute(self::COUNTRY);
    }

    public function getCountryId()
    {
        return 1;
    }

    public function getCreatedAt()
    {
        return $this->getAttribute(self::CREATED_AT);
    }

    public function getCurrencyId()
    {
        return 1;
    }

    public function getDate()
    {
        return '';
    }

    public function getFloor()
    {
        $floor = $this->getAttribute(self::FLOOR);
        if ($floor !== 'Otros' && $floor) {
            return (string) $floor;
        }
        return '';
    }

    public function getFurnished()
    {
        return 'No';
    }

    public function getGarages()
    {
        return (float) $this->getAttribute(self::GARAGES);
    }

    public function getLocationId()
    {
        return 0;
    }

    public function getLogSalePrice()
    {
        return 0;
    }

    public function getMap()
    {
        if ($this->getLatitude() && $this->getLongitude()) {
            return "{$this->getLatitude()},{$this->getLongitude()}";
        }
        return '';
    }

    public function getMapPublishOption()
    {
        return Property::MAP_PUBLISH_OPTIONS_PUBLISH_EXACT_POINT;
    }

    public function getMaxDate()
    {
        return '';
    }

    public function getLatitude()
    {
        return $this->getAttribute(self::LATITUDE);
    }

    public function getLongitude()
    {
        return $this->getAttribute(self::LONGITUDE);
    }

    public function getNeighborhood()
    {
        return $this->getAttribute(self::NEIGHBORHOOD);
    }

    public function getObservations()
    {
        return $this->getDescription();
    }

    public function getPrivateArea()
    {
        return $this->getAttribute(self::PRIVATE_AREA);
    }

    public function getPrivateAreaValue()
    {
        list($value) = explode(' ', $this->getPrivateArea());
        return (string) $value;
    }

    public function getPrivateAreaUnit()
    {
        if (!$this->getPrivateArea()) {
            return Property::UNIT_M2;
        }
        list($value, $unit) = explode(' ', $this->getPrivateArea());
        if ($unit === 'm²') {
            return Property::UNIT_M2;
        }
        if ($unit === 'Ha') {
            return Property::UNIT_HECTARES;
        }
        return ucfirst($unit);
    }

    public function getRating()
    {
        $rating = $this->getAttribute(self::RATING);
        if (is_numeric($rating)) {
            return $rating;
        }
        if ($rating === 'Campestre') {
            return Property::RATING_RURAL;
        }
        if ($rating === 'Comercial') {
            return Property::RATING_COMMERCIAL;
        }
        return '';
    }

    public function getReference()
    {
        return $this->getAttribute(self::REFERENCE);
    }

    public function getRegion()
    {
        return $this->getAttribute(self::REGION);
    }

    public function getRegionId()
    {
        /** @var \App\Contracts\Repository\FincaraizRegionRepositoryInterface $fincaraizRegionRepository */
        $fincaraizRegionRepository = app(FincaraizRegionRepositoryInterface::class);
        return (int) $fincaraizRegionRepository->getWasiRegionId($this->getRegion());
    }

    public function getRentPrice()
    {
        if ($this->getTradeType() === self::TRADE_TYPE_RENT) {
            return (string) $this->getAttribute(self::PRICE);
        }
        return '0';
    }

    public function getRentType()
    {
        return Property::RENT_TYPE_MONTHLY;
    }

    public function getSalePrice()
    {
        if ($this->getTradeType() === self::TRADE_TYPE_SALE) {
            return (float) $this->getAttribute(self::PRICE);
        }
        return 0.0;
    }

    public function getSharedInNetwork()
    {
        return '0';
    }

    public function getSoldPrice()
    {
        return 0.0;
    }

    public function getStatus()
    {
        return Property::STATUS_ACTIVE;
    }

    public function getTitle()
    {
        return (string) $this->getAttribute(self::TITLE);
    }

    public function getTradeType()
    {
        return $this->getAttribute(self::TRADE_TYPE);
    }

    public function getTradeTypeId()
    {
        if ($this->getTradeType() === self::TRADE_TYPE_SALE) {
            return 1;
        }
        if ($this->getTradeType() === self::TRADE_TYPE_RENT) {
            return 2;
        }
        return 1;
    }

    public function getType()
    {
        return $this->getAttribute(self::TYPE);
    }

    public function getTypeId()
    {
        switch ($this->getType()) {
            case 'Apartaestudio':
                return PropertyType::STUDIO_APARTMENT;
            case 'Apartamento':
                return PropertyType::APARTMENT;
            case 'Bodega':
                return PropertyType::WAREHOUSE;
            case 'Bodega Almacenamiento':
                return PropertyType::GARAGE;
            case 'Bodega Industrial':
                return PropertyType::INDUSTRIAL_WAREHOUSE;
            case 'Cabaña':
                return PropertyType::COTTAGE;
            case 'Casa':
            case 'Casa Lote':
                return PropertyType::HOUSE;
            case 'Casa Campestre':
                return PropertyType::COUNTRY_HOUSE;
            case 'Consultorio':
                return PropertyType::CONSULTING_ROOM;
            case 'Edificio':
                return PropertyType::BUILDING;
            case 'Finca':
                return PropertyType::ESTATE;
            case 'Finca Descanso':
                return PropertyType::CHALET;
            case 'Finca Producción':
                return PropertyType::FARMHOUSE;
            case 'Habitacion':
                return PropertyType::HOTEL;
            case 'Local':
                return PropertyType::COMMERCIAL_PROPERTY;
            case 'Lote':
                return PropertyType::LAND;
            case 'Oficina':
                return PropertyType::OFFICE;
            case 'Parqueadero':
                return PropertyType::GARAGE;
            case 'Otro':
            default:
                return PropertyType::HOUSE;
        }
    }

    public function getUpdatedAt()
    {
        return $this->getAttribute(self::UPDATED_AT);
    }

    public function getVideo()
    {
        return $this->getAttribute(self::VIDEO);
    }

    public function getVisits()
    {
        return $this->getAttribute(self::VISITS);
    }

    public function getZipCode()
    {
        return '';
    }

    public function getZone()
    {
        return $this->getAttribute(self::ZONE);
    }

    public function getZoneId()
    {
        // return $this->getNeighborhood();
        return 0;
    }

    public function setAddress($value)
    {
        $this->setAttribute(self::ADDRESS, $value);
        return $this;
    }

    public function setAdministrationFee($value)
    {
        $this->setAttribute(self::ADMINISTRATION_FEE, $value);
        return $this;
    }

    public function setArea($value)
    {
        $this->setAttribute(self::AREA, $value);
        return $this;
    }

    public function setBathrooms($value)
    {
        $this->setAttribute(self::BATHROOMS, $value);
        return $this;
    }

    public function setBedrooms($value)
    {
        $this->setAttribute(self::BEDROOMS, $value);
        return $this;
    }

    public function setBuiltArea($value)
    {
        $this->setAttribute(self::BUILT_AREA, $value);
        return $this;
    }

    public function setCity($value)
    {
        $this->setAttribute(self::CITY, $value);
        return $this;
    }

    public function setCondition($value)
    {
        $this->setAttribute(self::CONDITION, $value);
        return $this;
    }

    public function setCountry($value)
    {
        $this->setAttribute(self::COUNTRY, $value);
        return $this;
    }

    public function setFloor($value)
    {
        $this->setAttribute(self::FLOOR, $value);
        return $this;
    }

    public function setGarages($value)
    {
        $this->setAttribute(self::GARAGES, $value);
        return $this;
    }

    public function setLatitude($value)
    {
        $this->setAttribute(self::LATITUDE, $value);
        return $this;
    }

    public function setLongitude($value)
    {
        $this->setAttribute(self::LONGITUDE, $value);
        return $this;
    }

    public function setNeighborhood($value)
    {
        $this->setAttribute(self::NEIGHBORHOOD, $value);
        return $this;
    }

    public function setPrice($value)
    {
        $this->setAttribute(self::PRICE, $value);
        return $this;
    }

    public function setPrivateArea($value)
    {
        $this->setAttribute(self::PRIVATE_AREA, $value);
        return $this;
    }

    public function setRating($value)
    {
        $this->setAttribute(self::RATING, $value);
        return $this;
    }

    public function setReference($value)
    {
        $this->setAttribute(self::REFERENCE, $value);
        return $this;
    }

    public function setRegion($value)
    {
        $this->setAttribute(self::REGION, $value);
        return $this;
    }

    public function setTitle($value)
    {
        $this->setAttribute(self::TITLE, $value);
        return $this;
    }

    public function setTradeType($value)
    {
        $this->setAttribute(self::TRADE_TYPE, $value);
        return $this;
    }

    public function setType($value)
    {
        $this->setAttribute(self::TYPE, $value);
        return $this;
    }

    public function setVisits($value)
    {
        $this->setAttribute(self::VISITS, $value);
        return $this;
    }

    public function setVideo($value)
    {
        $this->setAttribute(self::VIDEO, $value);
        return $this;
    }

    public function setZone($value)
    {
        $this->setAttribute(self::ZONE, $value);
        return $this;
    }
}
