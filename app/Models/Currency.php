<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    const COP = 1;
    const ARS = 2;
    const USD = 3;
    const MXN = 4;
}
