<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    const ID = 'id_tipo_inmueble';
    const NAME = 'tipo';
    const PUBLIC_NAME = 'publico';

    protected $table = 'tipo_inmuebles';
    protected $primaryKey = self::ID;
    public $timestamps = false;
    protected $casts = [
        self::ID => 'integer',
        self::NAME => 'string',
        self::PUBLIC_NAME => 'string',
    ];

    const HOUSE = 1;
    const APARTMENT = 2;
    const COMMERCIAL_PROPERTY = 3;
    const OFFICE = 4;
    const LAND = 5;
    const COMMERCIAL_LAND = 6;
    const ESTATE = 7;
    const WAREHOUSE = 8;
    const CHALET = 10;
    const COUNTRY_HOUSE = 11;
    const HOTEL = 12;
    const HOTEL_ESTATE = 13;
    const STUDIO_APARTMENT = 14;
    const CONSULTING_ROOM = 15;
    const BUILDING = 16;
    const BEACH_LAND = 17;
    const HOSTEL = 18;
    const CONDOMINIUM = 19;
    const DUPLEX = 20;
    const PENTHOUSE = 21;
    const BUNGALOW = 22;
    const INDUSTRIAL_WAREHOUSE = 23;
    const BEACH_HOUSE = 24;
    const FLOOR = 25;
    const GARAGE = 26;
    const FARMHOUSE = 27;
    const COTTAGE = 28;
    const ISLAND = 29;
    const INDUSTRIAL_BUILDING = 30;
    const QUINTAS = 31;
    const TERRAIN = 32;

    public function getName()
    {
        return $this->getAttribute(self::NAME);
    }
}
