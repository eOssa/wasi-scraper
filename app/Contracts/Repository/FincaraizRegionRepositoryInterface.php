<?php

namespace App\Contracts\Repository;

interface FincaraizRegionRepositoryInterface extends RepositoryInterface
{
    /**
     * Get the wasi region id of a fincaraiz region.
     *
     * @param string|int $regionName
     *
     * @return int|string
     */
    public function getWasiRegionId($regionName);
}
