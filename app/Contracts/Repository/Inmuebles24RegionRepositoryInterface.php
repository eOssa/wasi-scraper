<?php

namespace App\Contracts\Repository;

interface Inmuebles24RegionRepositoryInterface extends RepositoryInterface
{
    /**
     * Get the wasi region id of an inmuebles24 region.
     *
     * @param string|int $regionName
     *
     * @return int|string
     */
    public function getWasiRegionId($regionName);
}
