<?php

namespace App\Contracts\Repository;

interface FincaraizCityRepositoryInterface extends RepositoryInterface
{
    /**
     * Get the wasi city id of a fincaraiz city.
     *
     * @param string|int $cityName
     *
     * @return int|string
     */
    public function getWasiCityId($cityName);
}
