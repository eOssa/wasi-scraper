<?php

namespace App\Contracts\Repository;

interface Inmuebles24CityRepositoryInterface extends RepositoryInterface
{
    /**
     * Get the wasi city id of an inmuebles24 city.
     *
     * @param string $cityName
     * @param int $regionId
     *
     * @return int
     */
    public function getWasiCityId($cityName, $regionId);
}
