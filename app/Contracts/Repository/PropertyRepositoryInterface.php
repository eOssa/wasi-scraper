<?php

namespace App\Contracts\Repository;

interface PropertyRepositoryInterface extends RepositoryInterface
{
    /**
     * Verify if exists a property by the reference.
     *
     * @param string|int $referece
     *
     * @return bool
     */
    public function existsByReference($reference);
}
