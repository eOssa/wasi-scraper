<?php

namespace App\Providers;

use App\Contracts\Repository\RepositoryInterface;
use Illuminate\Support\{Arr, ServiceProvider, Str};
use ReflectionClass;
use Symfony\Component\Finder\Finder;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories(app_path('Contracts/Repository'));
    }

    /**
     * Register all the repositories in the given directory.
     *
     * @param array|string $paths
     *
     * @return void
     */
    private function registerRepositories($paths)
    {
        $paths = array_unique(Arr::wrap($paths));

        $paths = array_filter($paths, function ($path) {
            return is_dir($path);
        });

        if (empty($paths)) {
            return;
        }

        $namespace = $this->app->getNamespace();

        foreach ((new Finder())->in($paths)->files() as $repositoryInterface) {
            $repositoryInterface = $namespace . str_replace(
                ['/', '.php'],
                ['\\', ''],
                Str::after($repositoryInterface->getPathname(), realpath(app_path()) . DIRECTORY_SEPARATOR)
            );
            $repository = str_replace(
                ['\\Contracts', 'Interface', '\\Repository'],
                ['', '', '\\Repositories'],
                $repositoryInterface
            );
            if (!interface_exists($repositoryInterface) || !class_exists($repository)) {
                continue;
            }
            $extendsBaseRepository = is_subclass_of($repositoryInterface, RepositoryInterface::class);
            $isAbstract = (new ReflectionClass($repository))->isAbstract();
            $implementsInterface = (new ReflectionClass($repository))->implementsInterface($repositoryInterface);
            if ($extendsBaseRepository && !$isAbstract && $implementsInterface) {
                $this->app->bind($repositoryInterface, $repository);
            }
        }
    }
}
