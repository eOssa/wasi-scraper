<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RegionNotFound
{
    use Dispatchable, SerializesModels;

    public $subject = 'Región no encontrada';
    public $message = 'No se encontró la región.';
    public $propertyJson = '';
    public $externalJson = '';

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($propertyJson, $externalJson)
    {
        $this->propertyJson = $propertyJson;
        $this->externalJson = $externalJson;
    }
}
