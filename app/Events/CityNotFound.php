<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CityNotFound
{
    use Dispatchable, SerializesModels;

    public $subject = 'Ciudad no encontrada';
    public $message = 'No se encontró la ciudad.';
    public $propertyJson = '';
    public $externalJson = '';

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($propertyJson, $externalJson)
    {
        $this->propertyJson = $propertyJson;
        $this->externalJson = $externalJson;
    }
}
