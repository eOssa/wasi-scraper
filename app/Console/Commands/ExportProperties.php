<?php

namespace App\Console\Commands;

use App\Models\Property;
use Illuminate\Console\Command;

class ExportProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export-properties {--F|format=csv}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the properties in a specific format.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $format = $this->option('format');
        if ($format === 'csv') {
            $this->exportCsv();
        }
        $this->info('The properties was exported succefully');
        return 0;
    }

    private function exportCsv()
    {
        $fp = fopen(storage_path('app' . DIRECTORY_SEPARATOR . 'properties.csv'), 'w');
        $heading = array_keys(Property::first()->toArray());
        fputcsv($fp, $heading);
        $bar = $this->output->createProgressBar(Property::count());
        Property::chunk(100, function ($properties) use ($fp, $bar, $heading) {
            foreach ($properties as $property) {
                /** @var \App\Models\Property $property */
                $values = array_map(function ($value) {
                    if (is_array($value)) {
                        return json_encode($value);
                    }
                    if (gettype($value) === 'string') {
                        return str_replace("\n", '', $value);
                    }
                    return $value;
                }, array_values($property->only($heading)));
                fputcsv($fp, $values);
                $bar->advance();
            }
        });
        $bar->finish();
        fclose($fp);
    }
}
