<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $phpExecutable = "php";
        $artisan = base_path('artisan');
        $schedule->exec("$phpExecutable $artisan dusk tests/Browser/Inmuebles24Test.php --group terrain-sale-1,terrain-sale-3")
            ->withoutOverlapping(1440 * 7)
            ->sendOutputTo(storage_path('logs/dusk.log'), true);
        // $schedule->exec("$phpExecutable $artisan dusk tests/Browser/Inmuebles24Test.php --group apartment,terrain-sale-2,terrain-sale-4")
        //     ->withoutOverlapping(1440 * 7)
        //     ->sendOutputTo(storage_path('logs/dusk.log'), true);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
