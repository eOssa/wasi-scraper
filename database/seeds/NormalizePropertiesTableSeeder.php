<?php

use App\Models\{FincaraizProperty, Property};
use Illuminate\Database\Seeder;

class NormalizePropertiesTableSeeder extends Seeder
{
    public function run()
    {
        Property::chunk(100, function ($properties) {
            /** @var \App\Models\Property[] $properties */
            foreach ($properties as $property) {
                $fincaraizProperty = new FincaraizProperty($property->toArray());
                $property->setAddress($fincaraizProperty->getAddress())
                    ->setAdministrationFee($fincaraizProperty->getAdministrationFee())
                    ->setArea($fincaraizProperty->getAreaValue())
                    ->setAreaUnit($fincaraizProperty->getAreaUnit())
                    ->setAvailability($fincaraizProperty->getAvailability())
                    ->setBathrooms($fincaraizProperty->getBathrooms())
                    ->setBedrooms($fincaraizProperty->getBedrooms())
                    ->setBuiltArea($fincaraizProperty->getBuiltAreaValue())
                    ->setBuiltAreaUnit($fincaraizProperty->getBuiltAreaUnit())
                    ->setBuiltDate($fincaraizProperty->getBuiltDate())
                    ->setCityId($fincaraizProperty->getCityId())
                    ->setCommissionType($fincaraizProperty->getCommissionType())
                    ->setCommissionValue($fincaraizProperty->getCommissionValue())
                    ->setCondition($fincaraizProperty->getCondition())
                    ->setCountryId($fincaraizProperty->getCountryId())
                    ->setCreatedAt($fincaraizProperty->getCreatedAt())
                    ->setCurrencyId($fincaraizProperty->getCurrencyId())
                    ->setDate($fincaraizProperty->getDate())
                    ->setFloor($fincaraizProperty->getFloor())
                    ->setFurnished($fincaraizProperty->getFurnished())
                    ->setGarages($fincaraizProperty->getGarages())
                    ->setLocationId($fincaraizProperty->getLocationId())
                    ->setLogSalePrice($fincaraizProperty->getLogSalePrice())
                    ->setMap($fincaraizProperty->getMap())
                    ->setMapPublishOption($fincaraizProperty->getMapPublishOption())
                    ->setMaxDate($fincaraizProperty->getMaxDate())
                    ->setPrivateArea($fincaraizProperty->getPrivateAreaValue())
                    ->setPrivateAreaUnit($fincaraizProperty->getPrivateAreaUnit())
                    ->setRating($fincaraizProperty->getRating())
                    ->setReference('Fincaraiz:' . $fincaraizProperty->getReference())
                    ->setRegionId($fincaraizProperty->getRegionId())
                    ->setRentPrice($fincaraizProperty->getRentPrice())
                    ->setRentType($fincaraizProperty->getRentType())
                    ->setSalePrice($fincaraizProperty->getSalePrice())
                    ->setSharedInNetwork($fincaraizProperty->getSharedInNetwork())
                    ->setSoldPrice($fincaraizProperty->getSoldPrice())
                    ->setStatus($fincaraizProperty->getStatus())
                    ->setTitle($fincaraizProperty->getTitle())
                    ->setTradeTypeId($fincaraizProperty->getTradeTypeId())
                    ->setType('')
                    ->setTypeId($fincaraizProperty->getTypeId())
                    ->setUpdatedAt($fincaraizProperty->getUpdatedAt())
                    ->setVideo($fincaraizProperty->getVideo())
                    ->setVisits($fincaraizProperty->getVisits())
                    ->setZipCode($fincaraizProperty->getZipCode())
                    ->setZoneId($fincaraizProperty->getZoneId());
                $property->save();
            }
        });
    }
}
