<?php

use App\Models\Inmuebles24City;
use Facades\App\Models\Inmuebles24City as Inmuebles24CityFacade;
use Facades\App\Models\Inmuebles24Region;
use Illuminate\Database\{Migrations\Migration, Schema\Blueprint};
use Illuminate\Support\Facades\Schema;

class CreateInmuebles24CitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(Inmuebles24CityFacade::getConnectionName())
            ->create(Inmuebles24CityFacade::getTable(), function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger(Inmuebles24City::REGION_ID);
                $table->string(Inmuebles24City::NAME);

                $table->foreign(Inmuebles24City::REGION_ID)
                    ->references(Inmuebles24Region::getKeyName())
                    ->on(Inmuebles24Region::getTable());
            });
        if (($handler = fopen(__DIR__ . '/../seeds/inmuebles24_cities.csv', 'r')) !== FALSE) {
            while (($city = fgetcsv($handler)) !== FALSE) {
                Inmuebles24City::create([
                    Inmuebles24CityFacade::getQualifiedKeyName() => $city[0],
                    Inmuebles24City::REGION_ID => $city[1],
                    Inmuebles24City::NAME => $city[2],
                ]);
            }
            fclose($handler);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(Inmuebles24CityFacade::getConnectionName())
            ->dropIfExists(Inmuebles24CityFacade::getTable());
    }
}
