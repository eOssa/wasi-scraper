<?php

use App\Models\Inmuebles24Region;
use Facades\App\Models\Inmuebles24Region as Inmuebles24RegionFacade;
use Illuminate\Database\{Migrations\Migration, Schema\Blueprint};
use Illuminate\Support\Facades\Schema;

class CreateInmuebles24RegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(Inmuebles24RegionFacade::getConnectionName())
            ->create(Inmuebles24RegionFacade::getTable(), function (Blueprint $table) {
                $table->id();
                $table->string(Inmuebles24Region::NAME);
            });
        if (($handler = fopen(__DIR__ . '/../seeds/inmuebles24_regions.csv', 'r')) !== FALSE) {
            while (($region = fgetcsv($handler)) !== FALSE) {
                Inmuebles24Region::create([
                    Inmuebles24RegionFacade::getQualifiedKeyName() => $region[0],
                    Inmuebles24Region::NAME => $region[1],
                ]);
            }
            fclose($handler);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(Inmuebles24RegionFacade::getConnectionName())
            ->dropIfExists(Inmuebles24RegionFacade::getTable());
    }
}
